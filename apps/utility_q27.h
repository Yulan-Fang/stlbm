// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm_q27.h"
#include <fstream>
#include <iomanip>
#include <cmath>
#include <string>
#include <sstream>
#include <stdexcept>
#include <map>
#include <set>
#include <any>

template<class LBM>
double computeEnergy(LBM& lbm, Dim const& dim, typename LBM::CellData* lattice, CellType* flag)
{
    double energy = 0.;
    for (int iX = 0; iX < dim.nx; ++iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                if (flag[i] != CellType::bounce_back) {
                    auto[rho, v] = lbm.macro(lattice[i]);
                    energy += v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
                }
            }
        }
    }
    energy *= 0.5;
    return energy;
}

template<class LBM>
void saveFields(LBM& lbm, Dim dim, typename LBM::CellData* lattice, CellType* flag)
{
    std::ofstream fvx("vx.dat");
    std::ofstream fvy("vy.dat");
    std::ofstream fvz("vz.dat");
    std::ofstream fv("v.dat");
    std::ofstream frho("rho.dat");
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            size_t i = lbm.xyz_to_i(iX, iY, dim.nz / 2);
            auto [rho, v] = lbm.macro(lattice[i]);
            if (flag[i] == CellType::bounce_back) {
                rho = 1.0;
                v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                      lbm.f(i, 1) / (6. * lbm.t[1]),
                      lbm.f(i, 2) / (6. * lbm.t[2]) };
            }
            fvx << v[0] << " ";
            fvy << v[1] << " ";
            fvz << v[2] << " ";
            fv << std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]) << " ";
            frho << rho << " ";

        }
        fvx << "\n";
        fvy << "\n";
        fvz << "\n";
        fv << "\n";
        frho << "\n";
    }
}

template<class LBM>
void saveVtkFields(LBM& lbm, Dim dim, typename LBM::CellData* lattice, CellType* flag, int time_iter)
{
    std::stringstream fNameStream;
    fNameStream << "sol_" << std::setfill('0') << std::setw(7) << time_iter << ".vtk";
    std::string fName = fNameStream.str();
    std::ofstream resultFlux;
    resultFlux.open(fName.c_str());
    resultFlux << "# vtk DataFile Version 2.0" << std::endl ;
    resultFlux << "iteration " << time_iter << std::endl ;
    resultFlux << "ASCII" << std::endl ;
    resultFlux << std::endl ;
    resultFlux << "DATASET STRUCTURED_POINTS" << std::endl ;
    resultFlux << "DIMENSIONS " << dim.nx << " " << dim.ny << " " << dim.nz << std::endl ;
    resultFlux << "ORIGIN 0 0 0" << std::endl ;
    resultFlux << "SPACING " << double(1./dim.nx) << " " << double(1./dim.ny) << " " << double(1./dim.nz) << std::endl ;
    resultFlux << std::endl ;
    resultFlux << "POINT_DATA " << dim.nx*dim.ny*dim.nz << std::endl ;

    // Density
    resultFlux << "SCALARS Density float 1" << std::endl ;
    resultFlux << "LOOKUP_TABLE default" << std::endl ;
    for (int iZ = dim.nz-1; iZ >=0; --iZ) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iX = 0; iX < dim.nx; ++iX) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lattice[i]);
                if (flag[i] == CellType::bounce_back) {
                    rho = 1.0;
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                resultFlux << rho << " ";
            }
            resultFlux << std::endl ;
        }
        resultFlux << std::endl ;
    }
    resultFlux << std::endl ;

    // velocity_X
    resultFlux << "SCALARS velocity_X float 1" << std::endl ;
    resultFlux << "LOOKUP_TABLE default" << std::endl ;
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lattice[i]);
                if (flag[i] == CellType::bounce_back) {
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                resultFlux << v[0] << " ";
            }
            resultFlux << std::endl ;
        }
        resultFlux << std::endl ;
    }
    resultFlux << std::endl ;

    // velocity_Y
    resultFlux << "SCALARS velocity_Y float 1" << std::endl ;
    resultFlux << "LOOKUP_TABLE default" << std::endl ;
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lattice[i]);
                if (flag[i] == CellType::bounce_back) {
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                resultFlux << v[1] << " ";
            }
            resultFlux << std::endl ;
        }
        resultFlux << std::endl ;
    }
    resultFlux << std::endl ;

    // velocity_Z
    resultFlux << "SCALARS velocity_Z float 1" << std::endl ;
    resultFlux << "LOOKUP_TABLE default" << std::endl ;
    for (int iX = dim.nx - 1; iX >= 0; --iX) {
        for (int iY = 0; iY < dim.ny; ++iY) {
            for (int iZ = 0; iZ < dim.nz; ++iZ) {
                size_t i = lbm.xyz_to_i(iX, iY, iZ);
                auto [rho, v] = lbm.macro(lattice[i]);
                if (flag[i] == CellType::bounce_back) {
                    v = { lbm.f(i, 0) / (6. * lbm.t[0]),
                          lbm.f(i, 1) / (6. * lbm.t[1]),
                          lbm.f(i, 2) / (6. * lbm.t[2]) };
                }
                resultFlux << v[2] << " ";
            }
            resultFlux << std::endl ;
        }
        resultFlux << std::endl ;
    }
}

template<class LBM>
void saveProfiles(LBM& lbm, Dim dim, double ulb, typename LBM::CellData* lattice, CellType* flag)
{
    using namespace std;
    ofstream middlex("middlex.dat");
    ofstream middley("middley.dat");
    ofstream middlez("middlez.dat");
    int x1, x2, y1, y2, z1, z2;

    if (dim.nx % 2 == 0) {
        x1 = dim.nx / 2 - 1;
        x2 = dim.nx / 2;
    }
    else {
        x1 = x2 = (dim.nx - 1) / 2;
    }

    if (dim.ny % 2 == 0) {
        y1 = dim.ny / 2 - 1;
        y2 = dim.ny / 2;
    }
    else {
        y1 = y2 = (dim.ny - 1) / 2;
    }

    if (dim.nz % 2 == 0) {
        z1 = dim.nz / 2 - 1;
        z2 = dim.nz / 2;
    }
    else {
        z1 = z2 = (dim.nz - 1) / 2;
    }

    for (int iX = 1; iX < dim.nx - 1; ++iX) {
        size_t i1 = lbm.xyz_to_i(iX, y1, z1);
        size_t i2 = lbm.xyz_to_i(iX, y2, z2);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nx - 2);
            double pos = (static_cast<double>(iX) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            middlex << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }

    for (int iY = 1; iY < dim.ny - 1; ++iY) {
        size_t i1 = lbm.xyz_to_i(x1, iY, z1);
        size_t i2 = lbm.xyz_to_i(x2, iY, z2);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.ny - 2);
            double pos = (static_cast<double>(iY) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            middley << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }

    for (int iZ = 1; iZ < dim.nz - 1; ++iZ) {
        size_t i1 = lbm.xyz_to_i(x1, y1, iZ);
        size_t i2 = lbm.xyz_to_i(x2, y2, iZ);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nz - 2);
            double pos = (static_cast<double>(iZ) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            middlez << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << 0.5 * (v1[0] + v2[0]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[1] + v2[1]) / ulb
                    << setw(20) << setprecision(8) << 0.5 * (v1[2] + v2[2]) / ulb << "\n";
        }
    }
}

template<class LBM>
void saveProfiles(LBM& lbm, Dim dim, double ulb, typename LBM::CellData* lattice, CellType* flag,
                  std::vector<std::array<double, 3>>& xprof, std::vector<std::array<double, 3>>& yprof,
                  std::vector<std::array<double, 3>>& zprof, int num_profiles )
{
    using namespace std;
    ofstream middlex("middlex.dat");
    ofstream middley("middley.dat");
    ofstream middlez("middlez.dat");
    int x1, x2, y1, y2, z1, z2;

    if (dim.nx % 2 == 0) {
        x1 = dim.nx / 2 - 1;
        x2 = dim.nx / 2;
    }
    else {
        x1 = x2 = (dim.nx - 1) / 2;
    }

    if (dim.ny % 2 == 0) {
        y1 = dim.ny / 2 - 1;
        y2 = dim.ny / 2;
    }
    else {
        y1 = y2 = (dim.ny - 1) / 2;
    }

    if (dim.nz % 2 == 0) {
        z1 = dim.nz / 2 - 1;
        z2 = dim.nz / 2;
    }
    else {
        z1 = z2 = (dim.nz - 1) / 2;
    }

    for (int iX = 1; iX < dim.nx - 1; ++iX) {
        size_t i1 = lbm.xyz_to_i(iX, y1, z1);
        size_t i2 = lbm.xyz_to_i(iX, y2, z2);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nx - 2);
            double pos = (static_cast<double>(iX) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            double vx = 0.5 * (v1[0] + v2[0]) / ulb;
            double vy = 0.5 * (v1[1] + v2[1]) / ulb;
            double vz = 0.5 * (v1[2] + v2[2]) / ulb;
            xprof[iX - 1][0] += vx;
            xprof[iX - 1][1] += vy;
            xprof[iX - 1][2] += vz;
            vx = xprof[iX - 1][0] / static_cast<double>(num_profiles);
            vy = xprof[iX - 1][1] / static_cast<double>(num_profiles);
            vz = xprof[iX - 1][2] / static_cast<double>(num_profiles);
            middlex << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << vx
                    << setw(20) << setprecision(8) << vy
                    << setw(20) << setprecision(8) << vz << "\n";
        }
    }

    for (int iY = 1; iY < dim.ny - 1; ++iY) {
        size_t i1 = lbm.xyz_to_i(x1, iY, z1);
        size_t i2 = lbm.xyz_to_i(x2, iY, z2);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.ny - 2);
            double pos = (static_cast<double>(iY) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            double vx = 0.5 * (v1[0] + v2[0]) / ulb;
            double vy = 0.5 * (v1[1] + v2[1]) / ulb;
            double vz = 0.5 * (v1[2] + v2[2]) / ulb;
            yprof[iY - 1][0] += vx;
            yprof[iY - 1][1] += vy;
            yprof[iY - 1][2] += vz;
            vx = yprof[iY - 1][0] / static_cast<double>(num_profiles);
            vy = yprof[iY - 1][1] / static_cast<double>(num_profiles);
            vz = yprof[iY - 1][2] / static_cast<double>(num_profiles);
            middley << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << vx
                    << setw(20) << setprecision(8) << vy
                    << setw(20) << setprecision(8) << vz << "\n";
        }
    }

    for (int iZ = 1; iZ < dim.nz - 1; ++iZ) {
        size_t i1 = lbm.xyz_to_i(x1, y1, iZ);
        size_t i2 = lbm.xyz_to_i(x2, y2, iZ);
        if (flag[i1] != CellType::bounce_back && flag[i2] != CellType::bounce_back) {
            double width = static_cast<double>(dim.nz - 2);
            double pos = (static_cast<double>(iZ) - 0.5) / width * 2. - 1.;
            auto [rho1, v1] = lbm.macro(lattice[i1]);
            auto [rho2, v2] = lbm.macro(lattice[i2]);
            double vx = 0.5 * (v1[0] + v2[0]) / ulb;
            double vy = 0.5 * (v1[1] + v2[1]) / ulb;
            double vz = 0.5 * (v1[2] + v2[2]) / ulb;
            zprof[iZ - 1][0] += vx;
            zprof[iZ - 1][1] += vy;
            zprof[iZ - 1][2] += vz;
            vx = zprof[iZ - 1][0] / static_cast<double>(num_profiles);
            vy = zprof[iZ - 1][1] / static_cast<double>(num_profiles);
            vz = zprof[iZ - 1][2] / static_cast<double>(num_profiles);
            middlez << setw(20) << setprecision(8) << pos
                    << setw(20) << setprecision(8) << vx
                    << setw(20) << setprecision(8) << vy
                    << setw(20) << setprecision(8) << vz << "\n";
        }
    }
}

inline auto parse_configfile() {
    using namespace std;
    set<string> real_keywords { "Re", "ulb", "max_t", "start_avg_profiles" };
    set<string> int_keywords { "N", "out_freq", "vtk_freq", "data_freq", "bench_ini_iter", "bench_max_iter" };
    set<string> bool_keywords { "benchmark", "unrolled" };
    set<string> string_keywords { "lbModel", "structure" };
    size_t nparam = real_keywords.size() + int_keywords.size() + bool_keywords.size() + string_keywords.size();

    map<string, std::any> parameters;

    ifstream ifile("config");
    if (!ifile) {
        throw invalid_argument("Config file not found. It should be name \"config\", without extension, and placed in the current directory.");
    }

    ifile.ignore(numeric_limits<streamsize>::max(), '\n');
    while (ifile) {
        string keyword;
        if (!(ifile >> keyword)) {
            break;
        }
        if (real_keywords.find(keyword) != real_keywords.end()) {
            double value;
            ifile >> value;
            parameters[keyword] = value;
        }
        else if (int_keywords.find(keyword) != int_keywords.end()) {
            int value;
            ifile >> value;
            parameters[keyword] = value;
        }
        else if (bool_keywords.find(keyword) != bool_keywords.end()) {
            bool value;
            ifile >> boolalpha >> value;
            parameters[keyword] = value;
        }
        else if (string_keywords.find(keyword) != string_keywords.end()) {
            string value;
            ifile >> value;
            parameters[keyword] = value;
        }
        else {
            throw invalid_argument("Wrong Keyword in config file: " + keyword);
        }
        ifile.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    if (parameters.size() != nparam) {
        throw invalid_argument("Insufficient number of arguments");
    }
    try {
        string structureStr = any_cast<string>(parameters.at("structure"));
        parameters["structureStr"] = structureStr;
        parameters["structure"] = stringToDataStructure().at(structureStr);
    }
    catch(out_of_range const& e) {
        throw invalid_argument("Unknown data structure: " + any_cast<string>(parameters["structure"]));
    }
    try {
        string modelStr = any_cast<string>(parameters.at("lbModel"));
        parameters["lbModelStr"] = modelStr;
        parameters["lbModel"] = stringToLBModel().at(modelStr);
    }
    catch(out_of_range const& e) {
        throw invalid_argument("Unknown LB model: " + any_cast<string>(parameters["lbModel"]));
    }
    return parameters;
}

