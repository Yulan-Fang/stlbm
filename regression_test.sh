#!/usr/bin/bash

# ******************************************************************************
# STLBM SOFTWARE LIBRARY

# Copyright © 2020 University of Geneva
# Authors: Jonas Latt, Christophe Coreixas, Joël Beny
# Contact: Jonas.Latt@unige.ch

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# ******************************************************************************

cd out

# 1) Test all data structures for bgk and trt in unrolled / non-unrolled mode
unrolled_options="true false"
models="bgk trt"
structures="twopop_aos twopop_soa swap_aos swap_soa aa_aos aa_soa"

for unrolled_option in $unrolled_options; do
    echo "RUNNING UNROLLED OPTION: $unrolled_option"
    for model in $models; do
        echo "RUNNING MODEL: $model"
        for structure in $structures; do
            echo "RUNNING STRUCTURE: $structure"
            cp ../apps/config.regression ./config
            sed -i "s/^unrolled.*/unrolled $unrolled_option/" config
            sed -i "s/^lbModel.*/lbModel $model/" config
            sed -i "s/^structure.*/structure $structure/" config
            ./cavity
            if [ $? -eq 2 ]; then
                echo "REGRESSION FAILED"
                exit 1
            fi
        done
    done
done

# 2) Test all collision models in unrolled mode for twopop_soa and aa_soa
unrolled_options="true"
models="bgk_o4 rm hm cm chm k rr"
structures="twopop_soa aa_soa"
for unrolled_option in $unrolled_options; do
    echo "RUNNING UNROLLED OPTION: $unrolled_option"
    for model in $models; do
        echo "RUNNING MODEL: $model"
        for structure in $structures; do
            echo "RUNNING STRUCTURE: $structure"
            cp ../apps/config.regression ./config
            sed -i "s/^unrolled.*/unrolled $unrolled_option/" config
            sed -i "s/^lbModel.*/lbModel $model/" config
            sed -i "s/^structure.*/structure $structure/" config
            ./cavity
            if [ $? -eq 2 ]; then
                echo "REGRESSION FAILED"
                exit 1
            fi
        done
    done
done

echo "REGRESSION SUCCEEDED"
exit 0

