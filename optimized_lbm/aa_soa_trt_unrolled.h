// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

// Implementation of AA-pattern structure-of-array, for TRT, with aggressive loop unrolling.

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_trt_unrolled {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i, 10) + f(i, 13) + f(i, 14) + f(i, 15) + f(i, 16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i, 11) + f(i, 12) + f(i, 17) + f(i, 18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i, 14);
        double Y_P1 = f(i, 4) + f(i, 11) + f(i, 13) + f(i, 17) + f(i, 18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i, 16) + f(i, 18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i, 12) + f(i, 15) + f(i, 17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[0] + pop[3] + pop[4] + pop[5] + pop[6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[9] + pop[1] + pop[2] + pop[7] + pop[8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[1] + pop[3] + pop[7] + pop[8] + pop[14];
        double Y_P1 = pop[4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[2] + pop[5] + pop[7] + pop[16] + pop[18];
        double Z_P1 = pop[6] + pop[8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

};

struct Even : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideTrtUnrolled(int i, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double s1 = 1 - 0.5*(s_plus+s_minus);
        double s2 = 0.5*(-s_plus+s_minus);
        double s3 = 1 - s1;

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        double pop_in_00 = f(i, 0);
        double pop_in_01 = f(i, 1);
        double pop_in_02 = f(i, 2);
        double pop_in_03 = f(i, 3);
        double pop_in_04 = f(i, 4);
        double pop_in_05 = f(i, 5);
        double pop_in_06 = f(i, 6);
        double pop_in_07 = f(i, 7);
        double pop_in_08 = f(i, 8);

        double pop_in_opp_00 = f(i, 10);
        double pop_in_opp_01 = f(i, 11);
        double pop_in_opp_02 = f(i, 12);
        double pop_in_opp_03 = f(i, 13);
        double pop_in_opp_04 = f(i, 14);
        double pop_in_opp_05 = f(i, 15);
        double pop_in_opp_06 = f(i, 16);
        double pop_in_opp_07 = f(i, 17);
        double pop_in_opp_08 = f(i, 18);

        f(i, 10) = pop_in_00*s1 + (pop_in_opp_00 - eqopp_00)*s2 + eq_00*s3;
        f(i, 11) = pop_in_01*s1 + (pop_in_opp_01 - eqopp_01)*s2 + eq_01*s3;
        f(i, 12) = pop_in_02*s1 + (pop_in_opp_02 - eqopp_02)*s2 + eq_02*s3;
        f(i, 13) = pop_in_03*s1 + (pop_in_opp_03 - eqopp_03)*s2 + eq_03*s3;
        f(i, 14) = pop_in_04*s1 + (pop_in_opp_04 - eqopp_04)*s2 + eq_04*s3;
        f(i, 15) = pop_in_05*s1 + (pop_in_opp_05 - eqopp_05)*s2 + eq_05*s3;
        f(i, 16) = pop_in_06*s1 + (pop_in_opp_06 - eqopp_06)*s2 + eq_06*s3;
        f(i, 17) = pop_in_07*s1 + (pop_in_opp_07 - eqopp_07)*s2 + eq_07*s3;
        f(i, 18) = pop_in_08*s1 + (pop_in_opp_08 - eqopp_08)*s2 + eq_08*s3;

        f(i,  0) = pop_in_opp_00*s1 + (pop_in_00 - eq_00)*s2 + eqopp_00*s3;
        f(i,  1) = pop_in_opp_01*s1 + (pop_in_01 - eq_01)*s2 + eqopp_01*s3;
        f(i,  2) = pop_in_opp_02*s1 + (pop_in_02 - eq_02)*s2 + eqopp_02*s3;
        f(i,  3) = pop_in_opp_03*s1 + (pop_in_03 - eq_03)*s2 + eqopp_03*s3;
        f(i,  4) = pop_in_opp_04*s1 + (pop_in_04 - eq_04)*s2 + eqopp_04*s3;
        f(i,  5) = pop_in_opp_05*s1 + (pop_in_05 - eq_05)*s2 + eqopp_05*s3;
        f(i,  6) = pop_in_opp_06*s1 + (pop_in_06 - eq_06)*s2 + eqopp_06*s3;
        f(i,  7) = pop_in_opp_07*s1 + (pop_in_07 - eq_07)*s2 + eqopp_07*s3;
        f(i,  8) = pop_in_opp_08*s1 + (pop_in_08 - eq_08)*s2 + eqopp_08*s3;

        f(i,  9) = (1. - omega) * f(i, 9) + omega * eq_09;
    }


    void iterateTrtUnrolled(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[rho, u] = macro(f0);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
            collideTrtUnrolled(i, rho, u, usqr);
        }
    }

    void operator() (double& f0) {
        iterateTrtUnrolled(f0);
    }
};

struct Odd : public LBM {

    // Optimizations are based on: 
    // 1 - Computation of opposite equilibrium populations using symmetries
    // 2 - Precomputation of ck_u terms
    auto collideTrtUnrolled(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u, double usqr) {
        double ck_u03 = u[0] + u[1];
        double ck_u04 = u[0] - u[1];
        double ck_u05 = u[0] + u[2];
        double ck_u06 = u[0] - u[2];
        double ck_u07 = u[1] + u[2];
        double ck_u08 = u[1] - u[2];

        double s_plus = omega;
        double s_minus = 8. * (2. - s_plus) / (8. - s_plus);
        double s1 = 1 - 0.5*(s_plus+s_minus);
        double s2 = 0.5*(-s_plus+s_minus);
        double s3 = 1 - s1;

        double eq_00    = rho * t[ 0] * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr);
        double eq_01    = rho * t[ 1] * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr);
        double eq_02    = rho * t[ 2] * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr);
        double eq_03    = rho * t[ 3] * (1. - 3. * ck_u03 + 4.5 * ck_u03 * ck_u03 - usqr);
        double eq_04    = rho * t[ 4] * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr);
        double eq_05    = rho * t[ 5] * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr);
        double eq_06    = rho * t[ 6] * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr);
        double eq_07    = rho * t[ 7] * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr);
        double eq_08    = rho * t[ 8] * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr);
        double eq_09    = rho * t[ 9] * (1. - usqr);
        double eqopp_00 = eq_00 + rho * t[ 0] * 6. * u[0];
        double eqopp_01 = eq_01 + rho * t[ 1] * 6. * u[1];
        double eqopp_02 = eq_02 + rho * t[ 2] * 6. * u[2];
        double eqopp_03 = eq_03 + rho * t[ 3] * 6. * ck_u03;
        double eqopp_04 = eq_04 + rho * t[ 4] * 6. * ck_u04;
        double eqopp_05 = eq_05 + rho * t[ 5] * 6. * ck_u05;
        double eqopp_06 = eq_06 + rho * t[ 6] * 6. * ck_u06;
        double eqopp_07 = eq_07 + rho * t[ 7] * 6. * ck_u07;
        double eqopp_08 = eq_08 + rho * t[ 8] * 6. * ck_u08;

        double pop_in_00 = pop[ 0];
        double pop_in_01 = pop[ 1];
        double pop_in_02 = pop[ 2];
        double pop_in_03 = pop[ 3];
        double pop_in_04 = pop[ 4];
        double pop_in_05 = pop[ 5];
        double pop_in_06 = pop[ 6];
        double pop_in_07 = pop[ 7];
        double pop_in_08 = pop[ 8];

        double pop_in_opp_00 = pop[10];
        double pop_in_opp_01 = pop[11];
        double pop_in_opp_02 = pop[12];
        double pop_in_opp_03 = pop[13];
        double pop_in_opp_04 = pop[14];
        double pop_in_opp_05 = pop[15];
        double pop_in_opp_06 = pop[16];
        double pop_in_opp_07 = pop[17];
        double pop_in_opp_08 = pop[18];
        
        pop[ 0] = pop_in_00*s1 + (pop_in_opp_00 - eqopp_00)*s2 + eq_00*s3;
        pop[ 1] = pop_in_01*s1 + (pop_in_opp_01 - eqopp_01)*s2 + eq_01*s3;
        pop[ 2] = pop_in_02*s1 + (pop_in_opp_02 - eqopp_02)*s2 + eq_02*s3;
        pop[ 3] = pop_in_03*s1 + (pop_in_opp_03 - eqopp_03)*s2 + eq_03*s3;
        pop[ 4] = pop_in_04*s1 + (pop_in_opp_04 - eqopp_04)*s2 + eq_04*s3;
        pop[ 5] = pop_in_05*s1 + (pop_in_opp_05 - eqopp_05)*s2 + eq_05*s3;
        pop[ 6] = pop_in_06*s1 + (pop_in_opp_06 - eqopp_06)*s2 + eq_06*s3;
        pop[ 7] = pop_in_07*s1 + (pop_in_opp_07 - eqopp_07)*s2 + eq_07*s3;
        pop[ 8] = pop_in_08*s1 + (pop_in_opp_08 - eqopp_08)*s2 + eq_08*s3;

        pop[10] = pop_in_opp_00*s1 + (pop_in_00 - eq_00)*s2 + eqopp_00*s3;
        pop[11] = pop_in_opp_01*s1 + (pop_in_01 - eq_01)*s2 + eqopp_01*s3;
        pop[12] = pop_in_opp_02*s1 + (pop_in_02 - eq_02)*s2 + eqopp_02*s3;
        pop[13] = pop_in_opp_03*s1 + (pop_in_03 - eq_03)*s2 + eqopp_03*s3;
        pop[14] = pop_in_opp_04*s1 + (pop_in_04 - eq_04)*s2 + eqopp_04*s3;
        pop[15] = pop_in_opp_05*s1 + (pop_in_05 - eq_05)*s2 + eqopp_05*s3;
        pop[16] = pop_in_opp_06*s1 + (pop_in_06 - eq_06)*s2 + eqopp_06*s3;
        pop[17] = pop_in_opp_07*s1 + (pop_in_07 - eq_07)*s2 + eqopp_07*s3;
        pop[18] = pop_in_opp_08*s1 + (pop_in_08 - eq_08)*s2 + eqopp_08*s3;

        pop[ 9] = (1. - omega) * pop[ 9] + omega * eq_09;
    }


    void iterateTrtUnrolled(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                int XX = iX - c[k][0];
                int YY = iY - c[k][1];
                int ZZ = iZ - c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    pop[k] = f(i, k) + f(nb, opp[k]);
                }
                else {
                    pop[k] = f(nb, opp[k]);
                }
            }

            auto[rho, u] = macropop(pop);
            double usqr = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

            collideTrtUnrolled(pop, rho, u, usqr);

            for (int k = 0; k < 19; ++k) {
                int XX = iX + c[k][0];
                int YY = iY + c[k][1];
                int ZZ = iZ + c[k][2];
                size_t nb = xyz_to_i(XX, YY, ZZ);
                CellType nbCellType = flag[nb];
                if (nbCellType == CellType::bounce_back) {
                    f(i, opp[k]) = pop[k] + f(nb, k);
                }
                else {
                    f(nb, k) = pop[k];
                }
            }
        }
    }

    void operator() (double& f0) {
        iterateTrtUnrolled(f0);
    }
};

} // namespace aa_soa_trt_unrolled
