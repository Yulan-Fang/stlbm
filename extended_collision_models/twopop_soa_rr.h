// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_rr {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i, k) = t[k];
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    // We only need second-order moments for the RR procedure
    auto computeRR(double& f0, double const& rho) {

        auto i = &f0 - lattice;
        std::array<double, 19> RR;
        std::fill(RR.begin(), RR.end(), 0.);

        double cs2 = 1./3.;
        double Hxx;
        double Hyy;
        double Hzz;
        for (int k = 0; k<19; ++k) {
            Hxx = c[k][0] * c[k][0] - cs2;
            Hyy = c[k][1] * c[k][1] - cs2;
            Hzz = c[k][2] * c[k][2] - cs2;
            // Order 2
            RR[M200] += Hxx * fin(i,k);
            RR[M020] += Hyy * fin(i,k);
            RR[M002] += Hzz * fin(i,k);
            RR[M110] += c[k][0] * c[k][1] * fin(i,k);
            RR[M101] += c[k][0] * c[k][2] * fin(i,k);
            RR[M011] += c[k][1] * c[k][2] * fin(i,k);
        }

        double invRho = 1. / rho;
        RR[M200] *= invRho;
        RR[M020] *= invRho;
        RR[M002] *= invRho;
        RR[M110] *= invRho;
        RR[M101] *= invRho;
        RR[M011] *= invRho;

        return RR;
    }

    // We only need second-order moments for the RR procedure (optimized computation)
    auto computeRRopt(double& f0, double const& rho) {

        auto i = &f0 - lattice;
        std::array<double, 19> RR;
        std::fill(RR.begin(), RR.end(), 0.);

        double cs2 = 1./3.;
        double invRho = 1. / rho;

        RR[M200] = invRho * (fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6) + fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16));
        RR[M020] = invRho * (fin(i, 1) + fin(i, 3) + fin(i, 4) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,13) + fin(i,14) + fin(i,17) + fin(i,18));
        RR[M002] = invRho * (fin(i, 2) + fin(i, 5) + fin(i, 6) + fin(i, 7) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,16) + fin(i,17) + fin(i,18));
        
        RR[M200] -= cs2;
        RR[M020] -= cs2;
        RR[M002] -= cs2;

        RR[M110] = invRho * (fin(i, 3) - fin(i, 4) + fin(i,13) - fin(i,14));
        RR[M101] = invRho * (fin(i, 5) - fin(i, 6) + fin(i,15) - fin(i,16));
        RR[M011] = invRho * (fin(i, 7) - fin(i, 8) + fin(i,17) - fin(i,18));

        return RR;
    }

    // We only need up to second-order moments for the RR procedure
    // Further optimization through merge with the computation of macros
    auto computeRRopt2(double& f0) {
        auto i = &f0 - lattice;
        std::array<double, 19> RR;
        std::fill(RR.begin(), RR.end(), 0.);

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        // Order 0
        RR[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / RR[M000];

        // Order 1
        RR[M100] = invRho * (X_P1 - X_M1);
        RR[M010] = invRho * (Y_P1 - Y_M1); 
        RR[M001] = invRho * (Z_P1 - Z_M1);

        // Order 2
        double cs2 = 1./3.;

        RR[M200] = invRho * (X_M1 + X_P1);
        RR[M020] = invRho * (Y_M1 + Y_P1);
        RR[M002] = invRho * (Z_M1 + Z_P1);
        
        RR[M200] -= cs2;
        RR[M020] -= cs2;
        RR[M002] -= cs2;

        RR[M110] = invRho * (fin(i,3) - fin(i,4) + fin(i,13) - fin(i,14));
        RR[M101] = invRho * (fin(i,5) - fin(i,6) + fin(i,15) - fin(i,16));
        RR[M011] = invRho * (fin(i,7) - fin(i,8) + fin(i,17) - fin(i,18));

        return RR;
    }

    auto computeRReq(std::array<double, 3> const& u) {

        std::array<double, 19> RReq;

        // Order 2
        RReq[M200] = u[0] * u[0];
        RReq[M020] = u[1] * u[1];
        RReq[M002] = u[2] * u[2];
        RReq[M110] = u[0] * u[1];
        RReq[M101] = u[0] * u[2];
        RReq[M011] = u[1] * u[2];
        // Order 3
        RReq[M210] = RReq[M200] * u[1];
        RReq[M201] = RReq[M200] * u[2];
        RReq[M021] = RReq[M020] * u[2];
        RReq[M120] = RReq[M020] * u[0];
        RReq[M102] = RReq[M002] * u[0];
        RReq[M012] = RReq[M002] * u[1];
        // Order 4
        RReq[M220] = RReq[M200] * RReq[M020];
        RReq[M202] = RReq[M200] * RReq[M002];
        RReq[M022] = RReq[M020] * RReq[M002];


        return RReq;
    }

    auto collideAndStreamRR(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u, std::array<double, 19> const& RR, std::array<double, 19> const& RReq) {

        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;//1.;
        const double omega4 = omega;//1.;

        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision and Nonequilibrium moments.
        std::array<double, 19> RRneq;
        std::array<double, 19> RRcoll;
        std::array<double, 19> RMcoll;

        // Recursive computation of nonequilibrium Hermite moments
        // Order 2 (standard way to compute them)
        RRneq[M200] = RR[M200] - RReq[M200];
        RRneq[M020] = RR[M020] - RReq[M020];
        RRneq[M002] = RR[M002] - RReq[M002];
        RRneq[M110] = RR[M110] - RReq[M110];
        RRneq[M101] = RR[M101] - RReq[M101];
        RRneq[M011] = RR[M011] - RReq[M011];

        // Order 3 (reconstruction using Chapman-Enskog formulas)
        RRneq[M210] = u[1]*RRneq[M200] + 2.*u[0]*RRneq[M110];
        RRneq[M201] = u[2]*RRneq[M200] + 2.*u[0]*RRneq[M101];
        RRneq[M021] = u[2]*RRneq[M020] + 2.*u[1]*RRneq[M011];
        RRneq[M120] = u[0]*RRneq[M020] + 2.*u[1]*RRneq[M110];
        RRneq[M102] = u[0]*RRneq[M002] + 2.*u[2]*RRneq[M101];
        RRneq[M012] = u[1]*RRneq[M002] + 2.*u[2]*RRneq[M011];

        // Order 4 (reconstruction using Chapman-Enskog formulas)
        RRneq[M220] = RReq[M020]*RRneq[M200] + RReq[M200]*RRneq[M020] + 4.*RReq[M110]*RRneq[M110];
        RRneq[M202] = RReq[M002]*RRneq[M200] + RReq[M200]*RRneq[M002] + 4.*RReq[M101]*RRneq[M101];
        RRneq[M022] = RReq[M002]*RRneq[M020] + RReq[M020]*RRneq[M002] + 4.*RReq[M011]*RRneq[M011];

        // Collision in the Hermite moment space
        // Order 2
        RRcoll[M200] = RR[M200] - omega1 * RRneq[M200];
        RRcoll[M020] = RR[M020] - omega1 * RRneq[M020];
        RRcoll[M002] = RR[M002] - omega1 * RRneq[M002];

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // RRcoll[M200] = RR[M200] - omegaPlus  * RRneq[M200] - omegaMinus * RRneq[M020] - omegaMinus * RRneq[M002] ;
        // RRcoll[M020] = RR[M020] - omegaMinus * RRneq[M200] - omegaPlus  * RRneq[M020] - omegaMinus * RRneq[M002] ;
        // RRcoll[M002] = RR[M002] - omegaMinus * RRneq[M200] - omegaMinus * RRneq[M020] - omegaPlus  * RRneq[M002] ;
        
        RRcoll[M110] = RR[M110] - omega2 * RRneq[M110];
        RRcoll[M101] = RR[M101] - omega2 * RRneq[M101];
        RRcoll[M011] = RR[M011] - omega2 * RRneq[M011];

        // Order 3 (Optimization: use RReq to avoid the computation of 3rd-order RR moments)
        RRcoll[M210] = RReq[M210] + (1. - omega3) * RRneq[M210];
        RRcoll[M201] = RReq[M201] + (1. - omega3) * RRneq[M201];
        RRcoll[M021] = RReq[M021] + (1. - omega3) * RRneq[M021];
        RRcoll[M120] = RReq[M120] + (1. - omega3) * RRneq[M120];
        RRcoll[M102] = RReq[M102] + (1. - omega3) * RRneq[M102];
        RRcoll[M012] = RReq[M012] + (1. - omega3) * RRneq[M012];

        // Order 4 (Optimization: use RReq to avoid the computation of 4th-order RR moments)
        RRcoll[M220] = RReq[M220] + (1. - omega4) * RRneq[M220];
        RRcoll[M202] = RReq[M202] + (1. - omega4) * RRneq[M202];
        RRcoll[M022] = RReq[M022] + (1. - omega4) * RRneq[M022];

        // Come back to RMcoll using relationships between RRs and RMs
        RMcoll[M200] = RRcoll[M200] + cs2;
        RMcoll[M020] = RRcoll[M020] + cs2;
        RMcoll[M002] = RRcoll[M002] + cs2;
        
        RMcoll[M110] = RRcoll[M110];
        RMcoll[M101] = RRcoll[M101];
        RMcoll[M011] = RRcoll[M011];

        RMcoll[M210] = RRcoll[M210] + cs2*u[1];
        RMcoll[M201] = RRcoll[M201] + cs2*u[2];
        RMcoll[M021] = RRcoll[M021] + cs2*u[2];
        RMcoll[M120] = RRcoll[M120] + cs2*u[0];
        RMcoll[M102] = RRcoll[M102] + cs2*u[0];
        RMcoll[M012] = RRcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = RRcoll[M220] + cs2*(RRcoll[M200] + RRcoll[M020]) + cs4;
        RMcoll[M202] = RRcoll[M202] + cs2*(RRcoll[M200] + RRcoll[M002]) + cs4;
        RMcoll[M022] = RRcoll[M022] + cs2*(RRcoll[M020] + RRcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_opp_00 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00     =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_opp_00;

        double pop_out_opp_01 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01     =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_opp_01;

        double pop_out_opp_02 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02     =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_opp_02;

        double pop_out_opp_03 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04     =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_opp_03;
        double pop_out_opp_04 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_opp_03;
        double pop_out_03     =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_opp_03;

        double pop_out_opp_05 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06     =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_opp_05;
        double pop_out_opp_06 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_opp_05;
        double pop_out_05     =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_opp_05;

        double pop_out_opp_07 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08     =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_opp_07;
        double pop_out_opp_08 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_opp_07;
        double pop_out_07     =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_opp_07;

        int XX = iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 10) = pop_out_00 + f(nb, 0);
        }
        else {
            fout(nb, 0) = pop_out_00;
        }

        XX = iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 0) = pop_out_opp_00 + f(nb,10);
        }
        else {
            fout(nb,10) = pop_out_opp_00;
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 11) = pop_out_01 + f(nb, 1);
        }
        else {
            fout(nb, 1) = pop_out_01;
        }

        XX = iX;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 1) = pop_out_opp_01 + f(nb,11);
        }
        else {
            fout(nb,11) = pop_out_opp_01;
        }

        XX = iX;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 12) = pop_out_02 + f(nb, 2);
        }
        else {
            fout(nb, 2) = pop_out_02;
        }

        XX = iX;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 2) = pop_out_opp_02 + f(nb,12);
        }
        else {
            fout(nb,12) = pop_out_opp_02;
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 13) = pop_out_03 + f(nb, 3);
        }
        else {
            fout(nb, 3) = pop_out_03;
        }

        XX = iX + 1;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 3) = pop_out_opp_03 + f(nb,13);
        }
        else {
            fout(nb,13) = pop_out_opp_03;
        }

        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 14) = pop_out_04 + f(nb, 4);
        }
        else {
            fout(nb, 4) = pop_out_04;
        }

        XX = iX + 1;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 4) = pop_out_opp_04 + f(nb,14);
        }
        else {
            fout(nb,14) = pop_out_opp_04;
        }

        XX = iX - 1;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 15) = pop_out_05 + f(nb, 5);
        }
        else {
            fout(nb, 5) = pop_out_05;
        }

        XX = iX + 1;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 5) = pop_out_opp_05 + f(nb,15);
        }
        else {
            fout(nb,15) = pop_out_opp_05;
        }

        XX = iX - 1;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 16) = pop_out_06 + f(nb, 6);
        }
        else {
            fout(nb, 6) = pop_out_06;
        }

        XX = iX + 1;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 6) = pop_out_opp_06 + f(nb,16);
        }
        else {
            fout(nb,16) = pop_out_opp_06;
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 17) = pop_out_07 + f(nb, 7);
        }
        else {
            fout(nb, 7) = pop_out_07;
        }

        XX = iX;
        YY = iY + 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 7) = pop_out_opp_07 + f(nb,17);
        }
        else {
            fout(nb,17) = pop_out_opp_07;
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 18) = pop_out_08 + f(nb, 8);
        }
        else {
            fout(nb, 8) = pop_out_08;
        }

        XX = iX;
        YY = iY + 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 8) = pop_out_opp_08 + f(nb,18);
        }
        else {
            fout(nb,18) = pop_out_opp_08;
        }


        fout(i, 9) =  pop_out_09;
    }


    void iterateRR(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            //auto[rho, u] = macro(f0);
            //auto RR = computeRRopt(f0, rho);
            auto RR = computeRRopt2(f0);
            double rho = RR[M000];
            std::array<double, 3> u = {RR[M100], RR[M010], RR[M001]};            
            auto RReq = computeRReq(u);
            collideAndStreamRR(i, iX, iY, iZ, rho, u, RR, RReq);
        }
    }

    void operator() (double& f0) {
        iterateRR(f0);
    }
};

} // namespace twopop_soa_rr

