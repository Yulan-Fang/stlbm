// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_cm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    auto computeCMopt(std::array<double, 19> const& pop, double const& rho, std::array<double, 3> const& u) {

        std::array<double, 19> CM;

        double invRho = 1./rho;
        // Optimized way to compute raw moments
        // Order 4
        CM[M220] = invRho * (pop[ 3] + pop[ 4] + pop[13] + pop[14]);
        CM[M202] = invRho * (pop[ 5] + pop[ 6] + pop[15] + pop[16]);
        CM[M022] = invRho * (pop[ 7] + pop[ 8] + pop[17] + pop[18]);
        // Order 2
        CM[M200] = invRho * (pop[ 0] + pop[10]) + CM[M220] + CM[M202];
        CM[M020] = invRho * (pop[ 1] + pop[11]) + CM[M220] + CM[M022];
        CM[M002] = invRho * (pop[ 2] + pop[12]) + CM[M202] + CM[M022];
        CM[M110] = CM[M220] - 2.*invRho * (pop[ 4] + pop[14]);
        CM[M101] = CM[M202] - 2.*invRho * (pop[ 6] + pop[16]);
        CM[M011] = CM[M022] - 2.*invRho * (pop[ 8] + pop[18]);
        // Order 3
        CM[M210] = CM[M220] - 2.*invRho * (pop[ 3] + pop[14]);
        CM[M201] = CM[M202] - 2.*invRho * (pop[ 5] + pop[16]);
        CM[M021] = CM[M022] - 2.*invRho * (pop[ 7] + pop[18]);
        CM[M120] = CM[M220] - 2.*invRho * (pop[ 3] + pop[ 4]);
        CM[M102] = CM[M202] - 2.*invRho * (pop[ 5] + pop[ 6]);
        CM[M012] = CM[M022] - 2.*invRho * (pop[ 7] + pop[ 8]);

        // Compute central moments from raw moments using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];

        CM[M200] -= ux2;
        CM[M020] -= uy2;
        CM[M002] -= uz2;
        
        CM[M110] -= uxy;
        CM[M101] -= uxz;
        CM[M011] -= uyz;

        CM[M210] -= (u[1]*CM[M200] + 2.*u[0]*CM[M110] + ux2*u[1]);
        CM[M201] -= (u[2]*CM[M200] + 2.*u[0]*CM[M101] + ux2*u[2]);
        CM[M021] -= (u[2]*CM[M020] + 2.*u[1]*CM[M011] + uy2*u[2]);
        CM[M120] -= (u[0]*CM[M020] + 2.*u[1]*CM[M110] + u[0]*uy2);
        CM[M102] -= (u[0]*CM[M002] + 2.*u[2]*CM[M101] + u[0]*uz2);
        CM[M012] -= (u[1]*CM[M002] + 2.*u[2]*CM[M011] + u[1]*uz2);
        
        CM[M220] -= (2.*u[1]*CM[M210] + 2.*u[0]*CM[M120] + uy2*CM[M200] + ux2*CM[M020] + 4.*uxy*CM[M110] + ux2*uy2);
        CM[M202] -= (2.*u[2]*CM[M201] + 2.*u[0]*CM[M102] + uz2*CM[M200] + ux2*CM[M002] + 4.*uxz*CM[M101] + ux2*uz2);
        CM[M022] -= (2.*u[2]*CM[M021] + 2.*u[1]*CM[M012] + uz2*CM[M020] + uy2*CM[M002] + 4.*uyz*CM[M011] + uy2*uz2);

        return CM;
    }

    // Further optimization through merge with the computation of macros
    auto computeCMopt2(std::array<double, 19> const& pop) {
        std::array<double, 19> CM;
        std::fill(CM.begin(), CM.end(), 0.);

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        // Order 0
        CM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / CM[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        CM[M100] = invRho * (X_P1 - X_M1);
        CM[M010] = invRho * (Y_P1 - Y_M1); 
        CM[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        CM[M200] = invRho * (X_M1 + X_P1);
        CM[M020] = invRho * (Y_M1 + Y_P1);
        CM[M002] = invRho * (Z_M1 + Z_P1);
        CM[M110] = invRho * ( pop[ 3] - pop[ 4] + pop[13] - pop[14]);
        CM[M101] = invRho * ( pop[ 5] - pop[ 6] + pop[15] - pop[16]);
        CM[M011] = invRho * ( pop[ 7] - pop[ 8] + pop[17] - pop[18]);
        // Order 3
        CM[M210] = CM[M110] - two_invRho * (pop[ 3] - pop[ 4]);
        CM[M201] = CM[M101] - two_invRho * (pop[ 5] - pop[ 6]);
        CM[M021] = CM[M011] - two_invRho * (pop[ 7] - pop[ 8]);
        CM[M120] = CM[M110] - two_invRho * (pop[ 3] - pop[14]);
        CM[M102] = CM[M101] - two_invRho * (pop[ 5] - pop[16]);
        CM[M012] = CM[M011] - two_invRho * (pop[ 7] - pop[18]);
        // Order 4
        CM[M220] = CM[M110] + two_invRho * (pop[ 4] + pop[14]);
        CM[M202] = CM[M101] + two_invRho * (pop[ 6] + pop[16]);
        CM[M022] = CM[M011] + two_invRho * (pop[ 8] + pop[18]);

        // Compute central moments from raw moments using binomial formulas
        double ux2 = CM[M100]*CM[M100];
        double uy2 = CM[M010]*CM[M010];
        double uz2 = CM[M001]*CM[M001];
        double uxy = CM[M100]*CM[M010];
        double uxz = CM[M100]*CM[M001];
        double uyz = CM[M010]*CM[M001];

        CM[M200] -= ux2;
        CM[M020] -= uy2;
        CM[M002] -= uz2;
        
        CM[M110] -= uxy;
        CM[M101] -= uxz;
        CM[M011] -= uyz;

        CM[M210] -= (CM[M010]*CM[M200] + 2.*CM[M100]*CM[M110] + ux2*CM[M010]);
        CM[M201] -= (CM[M001]*CM[M200] + 2.*CM[M100]*CM[M101] + ux2*CM[M001]);
        CM[M021] -= (CM[M001]*CM[M020] + 2.*CM[M010]*CM[M011] + uy2*CM[M001]);
        CM[M120] -= (CM[M100]*CM[M020] + 2.*CM[M010]*CM[M110] + CM[M100]*uy2);
        CM[M102] -= (CM[M100]*CM[M002] + 2.*CM[M001]*CM[M101] + CM[M100]*uz2);
        CM[M012] -= (CM[M010]*CM[M002] + 2.*CM[M001]*CM[M011] + CM[M010]*uz2);
        
        CM[M220] -= (2.*CM[M010]*CM[M210] + 2.*CM[M100]*CM[M120] + uy2*CM[M200] + ux2*CM[M020] + 4.*uxy*CM[M110] + ux2*uy2);
        CM[M202] -= (2.*CM[M001]*CM[M201] + 2.*CM[M100]*CM[M102] + uz2*CM[M200] + ux2*CM[M002] + 4.*uxz*CM[M101] + ux2*uz2);
        CM[M022] -= (2.*CM[M001]*CM[M021] + 2.*CM[M010]*CM[M012] + uz2*CM[M020] + uy2*CM[M002] + 4.*uyz*CM[M011] + uy2*uz2);

        return CM;
    }

    // We only need second-order moments for the CM procedure
    auto computeCMeq() {

        std::array<double, 19> CMeq;

        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        // Order 2
        CMeq[M200] = cs2;
        CMeq[M020] = cs2;
        CMeq[M002] = cs2;
        CMeq[M110] = 0.;
        CMeq[M101] = 0.;
        CMeq[M011] = 0.;
        // Order 3
        CMeq[M210] = 0.;
        CMeq[M201] = 0.;
        CMeq[M021] = 0.;
        CMeq[M120] = 0.;
        CMeq[M102] = 0.;
        CMeq[M012] = 0.;
        // Order 4
        CMeq[M220] = cs4;
        CMeq[M202] = cs4;
        CMeq[M022] = cs4;

        return CMeq;
    }
};

struct Even : public LBM {

    auto collideCM(int i, double rho, std::array<double, 3> const& u, std::array<double, 19> const& CM, std::array<double, 19> const& CMeq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;//1.;
        const double omega4 = omega;//1.;

        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision moments.
        std::array<double, 19> CMcoll;
        std::array<double, 19> RMcoll;

        // Order 2
        CMcoll[M200] = (1. - omega1) * CM[M200] + omega1 * cs2;
        CMcoll[M020] = (1. - omega1) * CM[M020] + omega1 * cs2;
        CMcoll[M002] = (1. - omega1) * CM[M002] + omega1 * cs2;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // CMcoll[M200] = CM[M200] - omegaPlus  * (CM[M200]-CMeq[M200]) - omegaMinus * (CM[M020]-CMeq[M020]) - omegaMinus * (CM[M002]-CMeq[M002]) ;
        // CMcoll[M020] = CM[M020] - omegaMinus * (CM[M200]-CMeq[M200]) - omegaPlus  * (CM[M020]-CMeq[M020]) - omegaMinus * (CM[M002]-CMeq[M002]) ;
        // CMcoll[M002] = CM[M002] - omegaMinus * (CM[M200]-CMeq[M200]) - omegaMinus * (CM[M020]-CMeq[M020]) - omegaPlus  * (CM[M002]-CMeq[M002]) ;
        
        CMcoll[M110] = (1. - omega2) * CM[M110];
        CMcoll[M101] = (1. - omega2) * CM[M101];
        CMcoll[M011] = (1. - omega2) * CM[M011];

        // Order 3
        CMcoll[M210] = (1. - omega3) * CM[M210];
        CMcoll[M201] = (1. - omega3) * CM[M201];
        CMcoll[M021] = (1. - omega3) * CM[M021];
        CMcoll[M120] = (1. - omega3) * CM[M120];
        CMcoll[M102] = (1. - omega3) * CM[M102];
        CMcoll[M012] = (1. - omega3) * CM[M012];
        
        // Order 4
        CMcoll[M220] = (1. - omega4) * CM[M220] + omega4 * cs4;
        CMcoll[M202] = (1. - omega4) * CM[M202] + omega4 * cs4;
        CMcoll[M022] = (1. - omega4) * CM[M022] + omega4 * cs4;

        // Come back to RMcoll using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];


        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + uxy;
        RMcoll[M101] = CMcoll[M101] + uxz;
        RMcoll[M011] = CMcoll[M011] + uyz;

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*uxy*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*uxz*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*uyz*CMcoll[M011] + uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateCM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
/*            auto[rho, u] = macropop(pop);
            auto CM = computeCMopt(pop, rho, u);*/
            auto CM = computeCMopt2(pop);
            double rho = CM[M000];
            std::array<double, 3> u = {CM[M100], CM[M010], CM[M001]};
            auto CMeq = computeCMeq();
            collideCM(i, rho, u, CM, CMeq);
        }
    }

    void operator() (double& f0) {
        iterateCM(f0);
    }
};

struct Odd : public LBM {

    auto collideCM(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u,
                   std::array<double, 19> const& CM, std::array<double, 19> const& CMeq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;//1.;
        const double omega4 = omega;//1.;

        double cs2 = 1./3.;
        double cs4 = cs2*cs2;

        // Post-collision moments.
        std::array<double, 19> CMcoll;
        std::array<double, 19> RMcoll;

        // Order 2
        CMcoll[M200] = (1. - omega1) * CM[M200] + omega1 * cs2;
        CMcoll[M020] = (1. - omega1) * CM[M020] + omega1 * cs2;
        CMcoll[M002] = (1. - omega1) * CM[M002] + omega1 * cs2;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // CMcoll[M200] = CM[M200] - omegaPlus  * (CM[M200]-CMeq[M200]) - omegaMinus * (CM[M020]-CMeq[M020]) - omegaMinus * (CM[M002]-CMeq[M002]) ;
        // CMcoll[M020] = CM[M020] - omegaMinus * (CM[M200]-CMeq[M200]) - omegaPlus  * (CM[M020]-CMeq[M020]) - omegaMinus * (CM[M002]-CMeq[M002]) ;
        // CMcoll[M002] = CM[M002] - omegaMinus * (CM[M200]-CMeq[M200]) - omegaMinus * (CM[M020]-CMeq[M020]) - omegaPlus  * (CM[M002]-CMeq[M002]) ;
        
        CMcoll[M110] = (1. - omega2) * CM[M110];
        CMcoll[M101] = (1. - omega2) * CM[M101];
        CMcoll[M011] = (1. - omega2) * CM[M011];

        // Order 3
        CMcoll[M210] = (1. - omega3) * CM[M210];
        CMcoll[M201] = (1. - omega3) * CM[M201];
        CMcoll[M021] = (1. - omega3) * CM[M021];
        CMcoll[M120] = (1. - omega3) * CM[M120];
        CMcoll[M102] = (1. - omega3) * CM[M102];
        CMcoll[M012] = (1. - omega3) * CM[M012];
        
        // Order 4
        CMcoll[M220] = (1. - omega4) * CM[M220] + omega4 * cs4;
        CMcoll[M202] = (1. - omega4) * CM[M202] + omega4 * cs4;
        CMcoll[M022] = (1. - omega4) * CM[M022] + omega4 * cs4;

        // Come back to RMcoll using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];


        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + uxy;
        RMcoll[M101] = CMcoll[M101] + uxz;
        RMcoll[M011] = CMcoll[M011] + uyz;

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*uxy*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*uxz*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*uyz*CMcoll[M011] + uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;


        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        for (int k = 0; k < 19; ++k) {
            int XX = iX - c[k][0];
            int YY = iY - c[k][1];
            int ZZ = iZ - c[k][2];
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (nbCellType == CellType::bounce_back) {
                pop[k] = f(i, k) + f(nb, opp[k]);
            }
            else {
                pop[k] = f(nb, opp[k]);
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        for (int k = 0; k < 19; ++k) {
            int XX = iX + c[k][0];
            int YY = iY + c[k][1];
            int ZZ = iZ + c[k][2];
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (nbCellType == CellType::bounce_back) {
                f(i, opp[k]) = pop[k] + f(nb, k);
            }
            else {
                f(nb, k) = pop[k];
            }
        }
    }

    void iterateCM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*            auto[rho, u] = macropop(pop);
            auto CM = computeCMopt(pop, rho, u);*/
            auto CM = computeCMopt2(pop);
            double rho = CM[M000];
            std::array<double, 3> u = {CM[M100], CM[M010], CM[M001]};
            auto CMeq = computeCMeq();

            collideCM(pop, rho, u, CM, CMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateCM(f0);
    }
};

} // namespace aa_soa_cm
