// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_k {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    auto computeKopt(std::array<double, 19> const& pop, double const& rho, std::array<double, 3> const& u) {

        std::array<double, 19> K;
        // Optimized way to compute raw moments
        double invRho = 1./rho;
        // Order 4
        K[M220] = invRho * (pop[ 3] + pop[ 4] + pop[13] + pop[14]);
        K[M202] = invRho * (pop[ 5] + pop[ 6] + pop[15] + pop[16]);
        K[M022] = invRho * (pop[ 7] + pop[ 8] + pop[17] + pop[18]);
        // Order 2
        K[M200] = invRho * (pop[ 0] + pop[10]) + K[M220] + K[M202];
        K[M020] = invRho * (pop[ 1] + pop[11]) + K[M220] + K[M022];
        K[M002] = invRho * (pop[ 2] + pop[12]) + K[M202] + K[M022];
     
        K[M110] = K[M220] - 2.*invRho * (pop[ 4] + pop[14]);
        K[M101] = K[M202] - 2.*invRho * (pop[ 6] + pop[16]);
        K[M011] = K[M022] - 2.*invRho * (pop[ 8] + pop[18]);
        // Order 3
        K[M210] = K[M220] - 2.*invRho * (pop[ 3] + pop[14]);
        K[M201] = K[M202] - 2.*invRho * (pop[ 5] + pop[16]);
        K[M021] = K[M022] - 2.*invRho * (pop[ 7] + pop[18]);
        K[M120] = K[M220] - 2.*invRho * (pop[ 3] + pop[ 4]);
        K[M102] = K[M202] - 2.*invRho * (pop[ 5] + pop[ 6]);
        K[M012] = K[M022] - 2.*invRho * (pop[ 7] + pop[ 8]);

        // Compute central moments from raw moments using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];

        K[M200] -= ux2;
        K[M020] -= uy2;
        K[M002] -= uz2;
        
        K[M110] -= uxy;
        K[M101] -= uxz;
        K[M011] -= uyz;

        K[M210] -= (u[1]*K[M200] + 2.*u[0]*K[M110] + ux2*u[1]);
        K[M201] -= (u[2]*K[M200] + 2.*u[0]*K[M101] + ux2*u[2]);
        K[M021] -= (u[2]*K[M020] + 2.*u[1]*K[M011] + uy2*u[2]);
        K[M120] -= (u[0]*K[M020] + 2.*u[1]*K[M110] + u[0]*uy2);
        K[M102] -= (u[0]*K[M002] + 2.*u[2]*K[M101] + u[0]*uz2);
        K[M012] -= (u[1]*K[M002] + 2.*u[2]*K[M011] + u[1]*uz2);
        
        K[M220] -= (2.*u[1]*K[M210] + 2.*u[0]*K[M120] + uy2*K[M200] + ux2*K[M020] + 4.*uxy*K[M110] + ux2*uy2);
        K[M202] -= (2.*u[2]*K[M201] + 2.*u[0]*K[M102] + uz2*K[M200] + ux2*K[M002] + 4.*uxz*K[M101] + ux2*uz2);
        K[M022] -= (2.*u[2]*K[M021] + 2.*u[1]*K[M012] + uz2*K[M020] + uy2*K[M002] + 4.*uyz*K[M011] + uy2*uz2);

        // Compute cumulants from central moments
        K[M220] -= (K[M200]*K[M020] + 2.*K[M110]*K[M110]);
        K[M202] -= (K[M200]*K[M002] + 2.*K[M101]*K[M101]);
        K[M022] -= (K[M020]*K[M002] + 2.*K[M011]*K[M011]);

        return K;
    }

    // Further optimization through merge with the computation of macros
    auto computeKopt2(std::array<double, 19> const& pop) {
        std::array<double, 19> K;
        std::fill(K.begin(), K.end(), 0.);

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        // Order 0
        K[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / K[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        K[M100] = invRho * (X_P1 - X_M1);
        K[M010] = invRho * (Y_P1 - Y_M1); 
        K[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        K[M200] = invRho * (X_M1 + X_P1);
        K[M020] = invRho * (Y_M1 + Y_P1);
        K[M002] = invRho * (Z_M1 + Z_P1);
        K[M110] = invRho * ( pop[ 3] - pop[ 4] + pop[13] - pop[14]);
        K[M101] = invRho * ( pop[ 5] - pop[ 6] + pop[15] - pop[16]);
        K[M011] = invRho * ( pop[ 7] - pop[ 8] + pop[17] - pop[18]);
        // Order 3
        K[M210] = K[M110] - two_invRho * (pop[ 3] - pop[ 4]);
        K[M201] = K[M101] - two_invRho * (pop[ 5] - pop[ 6]);
        K[M021] = K[M011] - two_invRho * (pop[ 7] - pop[ 8]);
        K[M120] = K[M110] - two_invRho * (pop[ 3] - pop[14]);
        K[M102] = K[M101] - two_invRho * (pop[ 5] - pop[16]);
        K[M012] = K[M011] - two_invRho * (pop[ 7] - pop[18]);
        // Order 4
        K[M220] = K[M110] + two_invRho * (pop[ 4] + pop[14]);
        K[M202] = K[M101] + two_invRho * (pop[ 6] + pop[16]);
        K[M022] = K[M011] + two_invRho * (pop[ 8] + pop[18]);

        // Compute central moments from raw moments using binomial formulas
        double ux2 = K[M100]*K[M100];
        double uy2 = K[M010]*K[M010];
        double uz2 = K[M001]*K[M001];
        double uxy = K[M100]*K[M010];
        double uxz = K[M100]*K[M001];
        double uyz = K[M010]*K[M001];

        K[M200] -= ux2;
        K[M020] -= uy2;
        K[M002] -= uz2;
        
        K[M110] -= uxy;
        K[M101] -= uxz;
        K[M011] -= uyz;

        K[M210] -= (K[M010]*K[M200] + 2.*K[M100]*K[M110] + ux2*K[M010]);
        K[M201] -= (K[M001]*K[M200] + 2.*K[M100]*K[M101] + ux2*K[M001]);
        K[M021] -= (K[M001]*K[M020] + 2.*K[M010]*K[M011] + uy2*K[M001]);
        K[M120] -= (K[M100]*K[M020] + 2.*K[M010]*K[M110] + K[M100]*uy2);
        K[M102] -= (K[M100]*K[M002] + 2.*K[M001]*K[M101] + K[M100]*uz2);
        K[M012] -= (K[M010]*K[M002] + 2.*K[M001]*K[M011] + K[M010]*uz2);
        
        K[M220] -= (2.*K[M010]*K[M210] + 2.*K[M100]*K[M120] + uy2*K[M200] + ux2*K[M020] + 4.*uxy*K[M110] + ux2*uy2);
        K[M202] -= (2.*K[M001]*K[M201] + 2.*K[M100]*K[M102] + uz2*K[M200] + ux2*K[M002] + 4.*uxz*K[M101] + ux2*uz2);
        K[M022] -= (2.*K[M001]*K[M021] + 2.*K[M010]*K[M012] + uz2*K[M020] + uy2*K[M002] + 4.*uyz*K[M011] + uy2*uz2);

        // Compute cumulants from central moments
        K[M220] -= (K[M200]*K[M020] + 2.*K[M110]*K[M110]);
        K[M202] -= (K[M200]*K[M002] + 2.*K[M101]*K[M101]);
        K[M022] -= (K[M020]*K[M002] + 2.*K[M011]*K[M011]);

        return K;
    }

    // We only need second-order moments for the K procedure
    auto computeKeq() {

        std::array<double, 19> Keq;

        double cs2 = 1./3.;
        // Order 2
        Keq[M200] = cs2;
        Keq[M020] = cs2;
        Keq[M002] = cs2;
        Keq[M110] = 0.;
        Keq[M101] = 0.;
        Keq[M011] = 0.;
        // Order 3
        Keq[M210] = 0.;
        Keq[M201] = 0.;
        Keq[M021] = 0.;
        Keq[M120] = 0.;
        Keq[M102] = 0.;
        Keq[M012] = 0.;
        // Order 4
        Keq[M220] = 0.;
        Keq[M202] = 0.;
        Keq[M022] = 0.;

        return Keq;
    }
};

struct Even : public LBM {

    auto collideK(int i, double rho, std::array<double, 3> const& u, std::array<double, 19> const& K, std::array<double, 19> const& Keq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;
        const double omega4 = omega;

        double cs2 = 1./3.;

        // Post-collision moments.
        std::array<double, 19> Kcoll;
        std::array<double, 19> CMcoll;
        std::array<double, 19> RMcoll;

        // Order 2
        Kcoll[M200] = (1. - omega1) * K[M200] + omega1 * cs2;
        Kcoll[M020] = (1. - omega1) * K[M020] + omega1 * cs2;
        Kcoll[M002] = (1. - omega1) * K[M002] + omega1 * cs2;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // Kcoll[M200] = K[M200] - omegaPlus  * (K[M200]-Keq[M200]) - omegaMinus * (K[M020]-Keq[M020]) - omegaMinus * (K[M002]-Keq[M002]) ;
        // Kcoll[M020] = K[M020] - omegaMinus * (K[M200]-Keq[M200]) - omegaPlus  * (K[M020]-Keq[M020]) - omegaMinus * (K[M002]-Keq[M002]) ;
        // Kcoll[M002] = K[M002] - omegaMinus * (K[M200]-Keq[M200]) - omegaMinus * (K[M020]-Keq[M020]) - omegaPlus  * (K[M002]-Keq[M002]) ;
        
        Kcoll[M110] = (1. - omega2) * K[M110];
        Kcoll[M101] = (1. - omega2) * K[M101];
        Kcoll[M011] = (1. - omega2) * K[M011];

        // Order 3
        Kcoll[M210] = (1. - omega3) * K[M210];
        Kcoll[M201] = (1. - omega3) * K[M201];
        Kcoll[M021] = (1. - omega3) * K[M021];
        Kcoll[M120] = (1. - omega3) * K[M120];
        Kcoll[M102] = (1. - omega3) * K[M102];
        Kcoll[M012] = (1. - omega3) * K[M012];
        
        // Order 4
        Kcoll[M220] = (1. - omega4) * K[M220];
        Kcoll[M202] = (1. - omega4) * K[M202];
        Kcoll[M022] = (1. - omega4) * K[M022];

        // Come back to CMcoll modifying fourth- and higher-order post-collision cumulants
        CMcoll[M200] = Kcoll[M200];
        CMcoll[M020] = Kcoll[M020];
        CMcoll[M002] = Kcoll[M002];
        CMcoll[M110] = Kcoll[M110];
        CMcoll[M101] = Kcoll[M101];
        CMcoll[M011] = Kcoll[M011];
        
        CMcoll[M210] = Kcoll[M210];
        CMcoll[M201] = Kcoll[M201];
        CMcoll[M021] = Kcoll[M021];
        CMcoll[M120] = Kcoll[M120];
        CMcoll[M102] = Kcoll[M102];
        CMcoll[M012] = Kcoll[M012];
        
        CMcoll[M220] = Kcoll[M220] + Kcoll[M200]*Kcoll[M020] + 2.*Kcoll[M110]*Kcoll[M110];
        CMcoll[M202] = Kcoll[M202] + Kcoll[M200]*Kcoll[M002] + 2.*Kcoll[M101]*Kcoll[M101];
        CMcoll[M022] = Kcoll[M022] + Kcoll[M020]*Kcoll[M002] + 2.*Kcoll[M011]*Kcoll[M011];

        // Come back to RMcoll using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];


        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + uxy;
        RMcoll[M101] = CMcoll[M101] + uxz;
        RMcoll[M011] = CMcoll[M011] + uyz;

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*uxy*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*uxz*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*uyz*CMcoll[M011] + uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateK(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
/*            auto[rho, u] = macropop(pop);
            auto K = computeKopt(pop, rho, u);*/
            auto K = computeKopt2(pop);
            double rho = K[M000];
            std::array<double, 3> u = {K[M100], K[M010], K[M001]};
            auto Keq = computeKeq();
            collideK(i, rho, u, K, Keq);
        }
    }

    void operator() (double& f0) {
        iterateK(f0);
    }
};

struct Odd : public LBM {

    auto collideK(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u,
                  std::array<double, 19> const& K, std::array<double, 19> const& Keq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;
        const double omega4 = omega;

        double cs2 = 1./3.;

        // Post-collision moments.
        std::array<double, 19> Kcoll;
        std::array<double, 19> CMcoll;
        std::array<double, 19> RMcoll;

        // Order 2
        Kcoll[M200] = (1. - omega1) * K[M200] + omega1 * cs2;
        Kcoll[M020] = (1. - omega1) * K[M020] + omega1 * cs2;
        Kcoll[M002] = (1. - omega1) * K[M002] + omega1 * cs2;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // Kcoll[M200] = K[M200] - omegaPlus  * (K[M200]-Keq[M200]) - omegaMinus * (K[M020]-Keq[M020]) - omegaMinus * (K[M002]-Keq[M002]) ;
        // Kcoll[M020] = K[M020] - omegaMinus * (K[M200]-Keq[M200]) - omegaPlus  * (K[M020]-Keq[M020]) - omegaMinus * (K[M002]-Keq[M002]) ;
        // Kcoll[M002] = K[M002] - omegaMinus * (K[M200]-Keq[M200]) - omegaMinus * (K[M020]-Keq[M020]) - omegaPlus  * (K[M002]-Keq[M002]) ;
        
        Kcoll[M110] = (1. - omega2) * K[M110];
        Kcoll[M101] = (1. - omega2) * K[M101];
        Kcoll[M011] = (1. - omega2) * K[M011];

        // Order 3
        Kcoll[M210] = (1. - omega3) * K[M210];
        Kcoll[M201] = (1. - omega3) * K[M201];
        Kcoll[M021] = (1. - omega3) * K[M021];
        Kcoll[M120] = (1. - omega3) * K[M120];
        Kcoll[M102] = (1. - omega3) * K[M102];
        Kcoll[M012] = (1. - omega3) * K[M012];
        
        // Order 4
        Kcoll[M220] = (1. - omega4) * K[M220];
        Kcoll[M202] = (1. - omega4) * K[M202];
        Kcoll[M022] = (1. - omega4) * K[M022];

        // Come back to CMcoll modifying fourth- and higher-order post-collision cumulants
        CMcoll[M200] = Kcoll[M200];
        CMcoll[M020] = Kcoll[M020];
        CMcoll[M002] = Kcoll[M002];
        CMcoll[M110] = Kcoll[M110];
        CMcoll[M101] = Kcoll[M101];
        CMcoll[M011] = Kcoll[M011];
        
        CMcoll[M210] = Kcoll[M210];
        CMcoll[M201] = Kcoll[M201];
        CMcoll[M021] = Kcoll[M021];
        CMcoll[M120] = Kcoll[M120];
        CMcoll[M102] = Kcoll[M102];
        CMcoll[M012] = Kcoll[M012];
        
        CMcoll[M220] = Kcoll[M220] + Kcoll[M200]*Kcoll[M020] + 2.*Kcoll[M110]*Kcoll[M110];
        CMcoll[M202] = Kcoll[M202] + Kcoll[M200]*Kcoll[M002] + 2.*Kcoll[M101]*Kcoll[M101];
        CMcoll[M022] = Kcoll[M022] + Kcoll[M020]*Kcoll[M002] + 2.*Kcoll[M011]*Kcoll[M011];

        // Come back to RMcoll using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];


        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + uxy;
        RMcoll[M101] = CMcoll[M101] + uxz;
        RMcoll[M011] = CMcoll[M011] + uyz;

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*uxy*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*uxz*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*uyz*CMcoll[M011] + uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;


        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        for (int k = 0; k < 19; ++k) {
            int XX = iX - c[k][0];
            int YY = iY - c[k][1];
            int ZZ = iZ - c[k][2];
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (nbCellType == CellType::bounce_back) {
                pop[k] = f(i, k) + f(nb, opp[k]);
            }
            else {
                pop[k] = f(nb, opp[k]);
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        for (int k = 0; k < 19; ++k) {
            int XX = iX + c[k][0];
            int YY = iY + c[k][1];
            int ZZ = iZ + c[k][2];
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (nbCellType == CellType::bounce_back) {
                f(i, opp[k]) = pop[k] + f(nb, k);
            }
            else {
                f(nb, k) = pop[k];
            }
        }
    }

    void iterateK(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*            auto[rho, u] = macropop(pop);
            auto K = computeKopt(pop, rho, u);*/
            auto K = computeKopt2(pop);
            double rho = K[M000];
            std::array<double, 3> u = {K[M100], K[M010], K[M001]};
            auto Keq = computeKeq();

            collideK(pop, rho, u, K, Keq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateK(f0);
    }
};

} // namespace aa_soa_k
