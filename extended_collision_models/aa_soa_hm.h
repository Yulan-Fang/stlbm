// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>

namespace aa_soa_hm {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            f(i, k) = t[k];
        }
    };

    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = f(i, 0) + f(i, 3) + f(i, 4) + f(i, 5) + f(i, 6);
        double X_P1 = f(i,10) + f(i,13) + f(i,14) + f(i,15) + f(i,16);
        double X_0  = f(i, 9) + f(i, 1) + f(i, 2) + f(i, 7) + f(i, 8) + f(i,11) + f(i,12) + f(i,17) + f(i,18);

        double Y_M1 = f(i, 1) + f(i, 3) + f(i, 7) + f(i, 8) + f(i,14);
        double Y_P1 = f(i, 4) + f(i,11) + f(i,13) + f(i,17) + f(i,18);

        double Z_M1 = f(i, 2) + f(i, 5) + f(i, 7) + f(i,16) + f(i,18);
        double Z_P1 = f(i, 6) + f(i, 8) + f(i,12) + f(i,15) + f(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    }

    auto macropop (std::array<double, 19> const& pop) {
        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return make_pair(rho, u);
    };

    auto computeHMopt(std::array<double, 19> const& pop, double const& rho, std::array<double, 3> const& u) {

        std::array<double, 19> HM;

        double invRho = 1./rho;
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        
        // Optimized way to compute raw moments
        // Order 4
        HM[M220] = invRho * (pop[ 3] + pop[ 4] + pop[13] + pop[14]);
        HM[M202] = invRho * (pop[ 5] + pop[ 6] + pop[15] + pop[16]);
        HM[M022] = invRho * (pop[ 7] + pop[ 8] + pop[17] + pop[18]);
        // Order 2
        HM[M200] = invRho * (pop[ 0] + pop[10]) + HM[M220] + HM[M202];
        HM[M020] = invRho * (pop[ 1] + pop[11]) + HM[M220] + HM[M022];
        HM[M002] = invRho * (pop[ 2] + pop[12]) + HM[M202] + HM[M022];
        HM[M110] = HM[M220] - 2.*invRho * (pop[ 4] + pop[14]);
        HM[M101] = HM[M202] - 2.*invRho * (pop[ 6] + pop[16]);
        HM[M011] = HM[M022] - 2.*invRho * (pop[ 8] + pop[18]);
        // Order 3
        HM[M210] = HM[M220] - 2.*invRho * (pop[ 3] + pop[14]);
        HM[M201] = HM[M202] - 2.*invRho * (pop[ 5] + pop[16]);
        HM[M021] = HM[M022] - 2.*invRho * (pop[ 7] + pop[18]);
        HM[M120] = HM[M220] - 2.*invRho * (pop[ 3] + pop[ 4]);
        HM[M102] = HM[M202] - 2.*invRho * (pop[ 5] + pop[ 6]);
        HM[M012] = HM[M022] - 2.*invRho * (pop[ 7] + pop[ 8]);

        // We come back to Hermite moments
        HM[M200] -= cs2;
        HM[M020] -= cs2;
        HM[M002] -= cs2;

        HM[M210] -= cs2 * u[1];
        HM[M201] -= cs2 * u[2];
        HM[M021] -= cs2 * u[2];
        HM[M120] -= cs2 * u[0];
        HM[M102] -= cs2 * u[0];
        HM[M012] -= cs2 * u[1];

        HM[M220] -= (cs2*(HM[M200] + HM[M020]) + cs4);
        HM[M202] -= (cs2*(HM[M200] + HM[M002]) + cs4);
        HM[M022] -= (cs2*(HM[M020] + HM[M002]) + cs4);

        return HM;
    }

    // Further optimization through merge with the computation of macros
    auto computeHMopt2(std::array<double, 19> const& pop) {
        std::array<double, 19> HM;
        std::fill(HM.begin(), HM.end(), 0.);

        double X_M1 = pop[ 0] + pop[ 3] + pop[ 4] + pop[ 5] + pop[ 6];
        double X_P1 = pop[10] + pop[13] + pop[14] + pop[15] + pop[16];
        double X_0  = pop[ 9] + pop[ 1] + pop[ 2] + pop[ 7] + pop[ 8] + pop[11] + pop[12] + pop[17] + pop[18];

        double Y_M1 = pop[ 1] + pop[ 3] + pop[ 7] + pop[ 8] + pop[14];
        double Y_P1 = pop[ 4] + pop[11] + pop[13] + pop[17] + pop[18];

        double Z_M1 = pop[ 2] + pop[ 5] + pop[ 7] + pop[16] + pop[18];
        double Z_P1 = pop[ 6] + pop[ 8] + pop[12] + pop[15] + pop[17];

        // Order 0
        HM[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / HM[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        HM[M100] = invRho * (X_P1 - X_M1);
        HM[M010] = invRho * (Y_P1 - Y_M1); 
        HM[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        HM[M200] = invRho * (X_M1 + X_P1);
        HM[M020] = invRho * (Y_M1 + Y_P1);
        HM[M002] = invRho * (Z_M1 + Z_P1);
        HM[M110] = invRho * ( pop[ 3] - pop[ 4] + pop[13] - pop[14]);
        HM[M101] = invRho * ( pop[ 5] - pop[ 6] + pop[15] - pop[16]);
        HM[M011] = invRho * ( pop[ 7] - pop[ 8] + pop[17] - pop[18]);
        // Order 3
        HM[M210] = HM[M110] - two_invRho * (pop[ 3] - pop[ 4]);
        HM[M201] = HM[M101] - two_invRho * (pop[ 5] - pop[ 6]);
        HM[M021] = HM[M011] - two_invRho * (pop[ 7] - pop[ 8]);
        HM[M120] = HM[M110] - two_invRho * (pop[ 3] - pop[14]);
        HM[M102] = HM[M101] - two_invRho * (pop[ 5] - pop[16]);
        HM[M012] = HM[M011] - two_invRho * (pop[ 7] - pop[18]);
        // Order 4
        HM[M220] = HM[M110] + two_invRho * (pop[ 4] + pop[14]);
        HM[M202] = HM[M101] + two_invRho * (pop[ 6] + pop[16]);
        HM[M022] = HM[M011] + two_invRho * (pop[ 8] + pop[18]);

        // We come back to Hermite moments
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        HM[M200] -= cs2;
        HM[M020] -= cs2;
        HM[M002] -= cs2;

        HM[M210] -= cs2 * HM[M010];
        HM[M201] -= cs2 * HM[M001];
        HM[M021] -= cs2 * HM[M001];
        HM[M120] -= cs2 * HM[M100];
        HM[M102] -= cs2 * HM[M100];
        HM[M012] -= cs2 * HM[M010];

        HM[M220] -= (cs2*(HM[M200] + HM[M020]) + cs4);
        HM[M202] -= (cs2*(HM[M200] + HM[M002]) + cs4);
        HM[M022] -= (cs2*(HM[M020] + HM[M002]) + cs4);

        return HM;
    }

    // We only need second-order moments for the HM procedure
    auto computeHMeq(std::array<double, 3> const& u) {

        std::array<double, 19> HMeq;

        // Order 2
        HMeq[M200] = u[0] * u[0];
        HMeq[M020] = u[1] * u[1];
        HMeq[M002] = u[2] * u[2];
        HMeq[M110] = u[0] * u[1];
        HMeq[M101] = u[0] * u[2];
        HMeq[M011] = u[1] * u[2];
        // Order 3
        HMeq[M210] = HMeq[M200] * u[1];
        HMeq[M201] = HMeq[M200] * u[2];
        HMeq[M021] = HMeq[M020] * u[2];
        HMeq[M120] = HMeq[M020] * u[0];
        HMeq[M102] = HMeq[M002] * u[0];
        HMeq[M012] = HMeq[M002] * u[1];
        // Order 4
        HMeq[M220] = HMeq[M200] * HMeq[M020];
        HMeq[M202] = HMeq[M200] * HMeq[M002];
        HMeq[M022] = HMeq[M020] * HMeq[M002];

        return HMeq;
    }
};

struct Even : public LBM {

    auto collideHM(int i, double rho, std::array<double, 3> const& u, std::array<double, 19> const& HM, std::array<double, 19> const& HMeq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;//1.;
        const double omega4 = omega;//1.;

        // Post-collision  moments.
        std::array<double, 19> HMcoll;
        std::array<double, 19> RMcoll;

        // // Collision in the Hermite moment space
        // Order 2
        HMcoll[M200] = (1.-omega1) * HM[M200] + omega1 * HMeq[M200] ;
        HMcoll[M020] = (1.-omega1) * HM[M020] + omega1 * HMeq[M020] ;
        HMcoll[M002] = (1.-omega1) * HM[M002] + omega1 * HMeq[M002] ;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // HMcoll[M200] = HM[M200] - omegaPlus  * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
        // HMcoll[M020] = HM[M020] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaPlus  * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
        // HMcoll[M002] = HM[M002] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaPlus  * (HM[M002]-HMeq[M002]) ;

        HMcoll[M110] = (1.-omega2) * HM[M110] + omega2 * HMeq[M110] ;
        HMcoll[M101] = (1.-omega2) * HM[M101] + omega2 * HMeq[M101] ;
        HMcoll[M011] = (1.-omega2) * HM[M011] + omega2 * HMeq[M011] ;

        // Order 3
        HMcoll[M210] = (1.-omega3) * HM[M210] + omega3 * HMeq[M210] ;
        HMcoll[M201] = (1.-omega3) * HM[M201] + omega3 * HMeq[M201] ;
        HMcoll[M021] = (1.-omega3) * HM[M021] + omega3 * HMeq[M021] ;
        HMcoll[M120] = (1.-omega3) * HM[M120] + omega3 * HMeq[M120] ;
        HMcoll[M102] = (1.-omega3) * HM[M102] + omega3 * HMeq[M102] ;
        HMcoll[M012] = (1.-omega3) * HM[M012] + omega3 * HMeq[M012] ;
        
        // Order 4
        HMcoll[M220] = (1.-omega4) * HM[M220] + omega4 * HMeq[M220] ;
        HMcoll[M202] = (1.-omega4) * HM[M202] + omega4 * HMeq[M202] ;
        HMcoll[M022] = (1.-omega4) * HM[M022] + omega4 * HMeq[M022] ;


        // Come back to RMcoll using relationships between HMs and RMs
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;

        f(i,  0) = pop_out_10;
        f(i, 10) = pop_out_00;

        f(i,  1) = pop_out_11;
        f(i, 11) = pop_out_01;

        f(i,  2) = pop_out_12;
        f(i, 12) = pop_out_02;

        f(i,  3) = pop_out_13;
        f(i, 13) = pop_out_03;

        f(i,  4) = pop_out_14;
        f(i, 14) = pop_out_04;

        f(i,  5) = pop_out_15;
        f(i, 15) = pop_out_05;

        f(i,  6) = pop_out_16;
        f(i, 16) = pop_out_06;

        f(i,  7) = pop_out_17;
        f(i, 17) = pop_out_07;

        f(i,  8) = pop_out_18;
        f(i, 18) = pop_out_08;

        f(i, 9) = pop_out_09;
    }


    void iterateHM(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            std::array<double, 19> pop;
            for (int k = 0; k < 19; ++k) {
                pop[k] = f(i, k);
            }
/*            auto[rho, u] = macropop(pop);
            auto HM = computeHMopt(pop, rho);*/
            auto HM = computeHMopt2(pop);
            double rho = HM[M000];
            std::array<double, 3> u = {HM[M100], HM[M010], HM[M001]};
            auto HMeq = computeHMeq(u);
            collideHM(i, rho, u, HM, HMeq);
        }
    }

    void operator() (double& f0) {
        iterateHM(f0);
    }
};

struct Odd : public LBM {

    auto collideHM(std::array<double, 19>& pop, double rho, std::array<double, 3> const& u, std::array<double, 19> const& HM, std::array<double, 19> const& HMeq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;//1.;
        const double omega4 = omega;//1.;

        // Post-collision  moments.
        std::array<double, 19> HMcoll;
        std::array<double, 19> RMcoll;

        // // Collision in the Hermite moment space
        // Order 2
        HMcoll[M200] = (1.-omega1) * HM[M200] + omega1 * HMeq[M200] ;
        HMcoll[M020] = (1.-omega1) * HM[M020] + omega1 * HMeq[M020] ;
        HMcoll[M002] = (1.-omega1) * HM[M002] + omega1 * HMeq[M002] ;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // HMcoll[M200] = HM[M200] - omegaPlus  * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
        // HMcoll[M020] = HM[M020] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaPlus  * (HM[M020]-HMeq[M020]) - omegaMinus * (HM[M002]-HMeq[M002]) ;
        // HMcoll[M002] = HM[M002] - omegaMinus * (HM[M200]-HMeq[M200]) - omegaMinus * (HM[M020]-HMeq[M020]) - omegaPlus  * (HM[M002]-HMeq[M002]) ;

        HMcoll[M110] = (1.-omega2) * HM[M110] + omega2 * HMeq[M110] ;
        HMcoll[M101] = (1.-omega2) * HM[M101] + omega2 * HMeq[M101] ;
        HMcoll[M011] = (1.-omega2) * HM[M011] + omega2 * HMeq[M011] ;

        // Order 3
        HMcoll[M210] = (1.-omega3) * HM[M210] + omega3 * HMeq[M210] ;
        HMcoll[M201] = (1.-omega3) * HM[M201] + omega3 * HMeq[M201] ;
        HMcoll[M021] = (1.-omega3) * HM[M021] + omega3 * HMeq[M021] ;
        HMcoll[M120] = (1.-omega3) * HM[M120] + omega3 * HMeq[M120] ;
        HMcoll[M102] = (1.-omega3) * HM[M102] + omega3 * HMeq[M102] ;
        HMcoll[M012] = (1.-omega3) * HM[M012] + omega3 * HMeq[M012] ;
        
        // Order 4
        HMcoll[M220] = (1.-omega4) * HM[M220] + omega4 * HMeq[M220] ;
        HMcoll[M202] = (1.-omega4) * HM[M202] + omega4 * HMeq[M202] ;
        HMcoll[M022] = (1.-omega4) * HM[M022] + omega4 * HMeq[M022] ;


        // Come back to RMcoll using relationships between HMs and RMs
        double cs2 = 1./3.;
        double cs4 = cs2*cs2;
        RMcoll[M200] = HMcoll[M200] + cs2;
        RMcoll[M020] = HMcoll[M020] + cs2;
        RMcoll[M002] = HMcoll[M002] + cs2;
        
        RMcoll[M110] = HMcoll[M110];
        RMcoll[M101] = HMcoll[M101];
        RMcoll[M011] = HMcoll[M011];

        RMcoll[M210] = HMcoll[M210] + cs2*u[1];
        RMcoll[M201] = HMcoll[M201] + cs2*u[2];
        RMcoll[M021] = HMcoll[M021] + cs2*u[2];
        RMcoll[M120] = HMcoll[M120] + cs2*u[0];
        RMcoll[M102] = HMcoll[M102] + cs2*u[0];
        RMcoll[M012] = HMcoll[M012] + cs2*u[1];
        
        RMcoll[M220] = HMcoll[M220] + cs2*(HMcoll[M200] + HMcoll[M020]) + cs4;
        RMcoll[M202] = HMcoll[M202] + cs2*(HMcoll[M200] + HMcoll[M002]) + cs4;
        RMcoll[M022] = HMcoll[M022] + cs2*(HMcoll[M020] + HMcoll[M002]) + cs4;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_10 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00 =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_10;

        double pop_out_11 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01 =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_11;

        double pop_out_12 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02 =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_12;

        double pop_out_13 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04 =  0.5*rho * (-RMcoll[M110]              - RMcoll[M120]) + pop_out_13;
        double pop_out_14 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])              + pop_out_13;
        double pop_out_03 =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_13;

        double pop_out_15 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06 =  0.5*rho * (-RMcoll[M101]              - RMcoll[M102]) + pop_out_15;
        double pop_out_16 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])              + pop_out_15;
        double pop_out_05 =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_15;

        double pop_out_17 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08 =  0.5*rho * (-RMcoll[M011]              - RMcoll[M012]) + pop_out_17;
        double pop_out_18 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])              + pop_out_17;
        double pop_out_07 =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_17;


        pop[ 0] = pop_out_00;
        pop[10] = pop_out_10;

        pop[ 1] = pop_out_01;
        pop[11] = pop_out_11;

        pop[ 2] = pop_out_02;
        pop[12] = pop_out_12;

        pop[ 3] = pop_out_03;
        pop[13] = pop_out_13;

        pop[ 4] = pop_out_04;
        pop[14] = pop_out_14;

        pop[ 5] = pop_out_05;
        pop[15] = pop_out_15;

        pop[ 6] = pop_out_06;
        pop[16] = pop_out_16;

        pop[ 7] = pop_out_07;
        pop[17] = pop_out_17;

        pop[ 8] = pop_out_08;
        pop[18] = pop_out_18;

        pop[ 9] = pop_out_09;
    }

    auto streamingPull(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        for (int k = 0; k < 19; ++k) {
            int XX = iX - c[k][0];
            int YY = iY - c[k][1];
            int ZZ = iZ - c[k][2];
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (nbCellType == CellType::bounce_back) {
                pop[k] = f(i, k) + f(nb, opp[k]);
            }
            else {
                pop[k] = f(nb, opp[k]);
            }
        }
    }

    auto streamingPush(int i, int iX, int iY, int iZ, std::array<double, 19>& pop)
    {
        for (int k = 0; k < 19; ++k) {
            int XX = iX + c[k][0];
            int YY = iY + c[k][1];
            int ZZ = iZ + c[k][2];
            size_t nb = xyz_to_i(XX, YY, ZZ);
            CellType nbCellType = flag[nb];
            if (nbCellType == CellType::bounce_back) {
                f(i, opp[k]) = pop[k] + f(nb, k);
            }
            else {
                f(nb, k) = pop[k];
            }
        }
    }

    void iterateHM(double& f0) {
        int i = &f0 - lattice;
        auto[iX, iY, iZ] = i_to_xyz(i);
        CellType cellType = flag[i];

        if (cellType == CellType::bulk) {
            std::array<double, 19> pop;

            streamingPull(i, iX, iY, iZ, pop);

/*            auto[rho, u] = macropop(pop);
            auto HM = computeHMopt(pop, rho);*/
            auto HM = computeHMopt2(pop);
            double rho = HM[M000];
            std::array<double, 3> u = {HM[M100], HM[M010], HM[M001]};
            auto HMeq = computeHMeq(u);

            collideHM(pop, rho, u, HM, HMeq);

            streamingPush(i, iX, iY, iZ, pop);
        }
    }

    void operator() (double& f0) {
        iterateHM(f0);
    }
};

} // namespace aa_soa_hm
