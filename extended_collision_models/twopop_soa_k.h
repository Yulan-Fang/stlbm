// *****************************************************************************
// STLBM SOFTWARE LIBRARY

// Copyright © 2020 University of Geneva
// Authors: Jonas Latt, Christophe Coreixas, Joël Beny
// Contact: Jonas.Latt@unige.ch

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// *****************************************************************************

#pragma once

#include "lbm.h"
#include <vector>
#include <array>
#include <tuple>
#include <iostream>

namespace twopop_soa_k {

struct LBM {
    using CellData = double;
    static size_t sizeOfLattice(size_t nelem) { return 2 * 19 * nelem; }

    CellData* lattice;
    CellType* flag;
    int* parity;
    std::array<int, 3>* c;
    int* opp;
    double* t;
    double omega;
    Dim dim;
    LBModel model;

    auto i_to_xyz (int i) {
        int iX = i / (dim.ny * dim.nz);
        int remainder = i % (dim.ny * dim.nz);
        int iY = remainder / dim.nz;
        int iZ = remainder % dim.nz;
        return std::make_tuple(iX, iY, iZ);
    };

    size_t xyz_to_i (int x, int y, int z) {
        return z + dim.nz * (y + dim.ny * x);
    };

    double& f (int i, int k) {
        return lattice[k * dim.nelem + i];
    }

    double& fin (int i, int k) {
        return lattice[*parity * dim.npop + k * dim.nelem + i];
    }
    
    double& fout (int i, int k) {
        return lattice[(1 - *parity) * dim.npop + k * dim.nelem + i];
    }

    auto iniLattice (double& f0) {
        auto i = &f0 - lattice;
        for (int k = 0; k < 19; ++k) {
            fin(i, k) = t[k];
        }
    };

    // Required for the nonregression based on computeEnergy() 
    auto macro (double& f0) {
        auto i = &f0 - lattice;
        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        double rho = X_M1 + X_P1 + X_0;
        std::array<double, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
        return std::make_pair(rho, u);
    }
 
    auto computeK(double& f0, double const& rho, std::array<double, 3> const& u) {

        auto i = &f0 - lattice;
        std::array<double, 19> K;
        std::fill(K.begin(), K.end(), 0.);

        for (int k = 0; k < 19; ++k) {
            double cMux = c[k][0]- u[0];
            double cMuy = c[k][1]- u[1];
            double cMuz = c[k][2]- u[2];
            // Order 2
            K[M200] += fin(i,k)*cMux*cMux;
            K[M020] += fin(i,k)*cMuy*cMuy;
            K[M002] += fin(i,k)*cMuz*cMuz;
            K[M110] += fin(i,k)*cMux*cMuy;
            K[M101] += fin(i,k)*cMux*cMuz;
            K[M011] += fin(i,k)*cMuy*cMuz;
            // Order 3
            K[M210] += fin(i,k)*cMux*cMux*cMuy;
            K[M201] += fin(i,k)*cMux*cMux*cMuz;
            K[M021] += fin(i,k)*cMuy*cMuy*cMuz;
            K[M120] += fin(i,k)*cMux*cMuy*cMuy;
            K[M102] += fin(i,k)*cMux*cMuz*cMuz;
            K[M012] += fin(i,k)*cMuy*cMuz*cMuz;
            // Order 4
            K[M220] += fin(i,k)*cMux*cMux*cMuy*cMuy;
            K[M202] += fin(i,k)*cMux*cMux*cMuz*cMuz;
            K[M022] += fin(i,k)*cMuy*cMuy*cMuz*cMuz;
        }

        double invRho = 1./rho;
        for (int k = 0; k<19; ++k) {
            K[k] *= invRho;
        }

        K[M220] -= (K[M200]*K[M020] + 2.*K[M110]*K[M110]);
        K[M202] -= (K[M200]*K[M002] + 2.*K[M101]*K[M101]);
        K[M022] -= (K[M020]*K[M002] + 2.*K[M011]*K[M011]);

        return K;
    }

    auto computeKopt(double& f0, double const& rho, std::array<double, 3> const& u) {

        auto i = &f0 - lattice;
        std::array<double, 19> K;
        double invRho = 1./rho;
        // Optimized way to compute raw moments
        // Order 4
        K[M220] = invRho * (fin(i, 3) + fin(i, 4) + fin(i,13) + fin(i,14));
        K[M202] = invRho * (fin(i, 5) + fin(i, 6) + fin(i,15) + fin(i,16));
        K[M022] = invRho * (fin(i, 7) + fin(i, 8) + fin(i,17) + fin(i,18));
        // Order 2
        K[M200] = invRho * (fin(i, 0) + fin(i,10)) + K[M220] + K[M202];
        K[M020] = invRho * (fin(i, 1) + fin(i,11)) + K[M220] + K[M022];
        K[M002] = invRho * (fin(i, 2) + fin(i,12)) + K[M202] + K[M022];
     
        K[M110] = K[M220] - 2.*invRho * (fin(i, 4) + fin(i,14));
        K[M101] = K[M202] - 2.*invRho * (fin(i, 6) + fin(i,16));
        K[M011] = K[M022] - 2.*invRho * (fin(i, 8) + fin(i,18));
        // Order 3
        K[M210] = K[M220] - 2.*invRho * (fin(i, 3) + fin(i,14));
        K[M201] = K[M202] - 2.*invRho * (fin(i, 5) + fin(i,16));
        K[M021] = K[M022] - 2.*invRho * (fin(i, 7) + fin(i,18));
        K[M120] = K[M220] - 2.*invRho * (fin(i, 3) + fin(i, 4));
        K[M102] = K[M202] - 2.*invRho * (fin(i, 5) + fin(i, 6));
        K[M012] = K[M022] - 2.*invRho * (fin(i, 7) + fin(i, 8));

        // Compute central moments from raw moments using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];

        K[M200] -= ux2;
        K[M020] -= uy2;
        K[M002] -= uz2;
        
        K[M110] -= uxy;
        K[M101] -= uxz;
        K[M011] -= uyz;

        K[M210] -= (u[1]*K[M200] + 2.*u[0]*K[M110] + ux2*u[1]);
        K[M201] -= (u[2]*K[M200] + 2.*u[0]*K[M101] + ux2*u[2]);
        K[M021] -= (u[2]*K[M020] + 2.*u[1]*K[M011] + uy2*u[2]);
        K[M120] -= (u[0]*K[M020] + 2.*u[1]*K[M110] + u[0]*uy2);
        K[M102] -= (u[0]*K[M002] + 2.*u[2]*K[M101] + u[0]*uz2);
        K[M012] -= (u[1]*K[M002] + 2.*u[2]*K[M011] + u[1]*uz2);
        
        K[M220] -= (2.*u[1]*K[M210] + 2.*u[0]*K[M120] + uy2*K[M200] + ux2*K[M020] + 4.*uxy*K[M110] + ux2*uy2);
        K[M202] -= (2.*u[2]*K[M201] + 2.*u[0]*K[M102] + uz2*K[M200] + ux2*K[M002] + 4.*uxz*K[M101] + ux2*uz2);
        K[M022] -= (2.*u[2]*K[M021] + 2.*u[1]*K[M012] + uz2*K[M020] + uy2*K[M002] + 4.*uyz*K[M011] + uy2*uz2);

        // Compute cumulants from central moments
        K[M220] -= (K[M200]*K[M020] + 2.*K[M110]*K[M110]);
        K[M202] -= (K[M200]*K[M002] + 2.*K[M101]*K[M101]);
        K[M022] -= (K[M020]*K[M002] + 2.*K[M011]*K[M011]);

        return K;
    }

    // Further optimization through merge with the computation of macros
    auto computeKopt2(double& f0) {

        auto i = &f0 - lattice;
        std::array<double, 19> K;

        double X_M1 = fin(i, 0) + fin(i, 3) + fin(i, 4) + fin(i, 5) + fin(i, 6);
        double X_P1 = fin(i,10) + fin(i,13) + fin(i,14) + fin(i,15) + fin(i,16);
        double X_0  = fin(i, 9) + fin(i, 1) + fin(i, 2) + fin(i, 7) + fin(i, 8) + fin(i,11) + fin(i,12) + fin(i,17) + fin(i,18);

        double Y_M1 = fin(i, 1) + fin(i, 3) + fin(i, 7) + fin(i, 8) + fin(i,14);
        double Y_P1 = fin(i, 4) + fin(i,11) + fin(i,13) + fin(i,17) + fin(i,18);

        double Z_M1 = fin(i, 2) + fin(i, 5) + fin(i, 7) + fin(i,16) + fin(i,18);
        double Z_P1 = fin(i, 6) + fin(i, 8) + fin(i,12) + fin(i,15) + fin(i,17);

        // Order 0
        K[M000] = X_M1 + X_P1 + X_0;
        double invRho = 1. / K[M000];
        double two_invRho= 2.*invRho;
        // Order 1
        K[M100] = invRho * (X_P1 - X_M1);
        K[M010] = invRho * (Y_P1 - Y_M1); 
        K[M001] = invRho * (Z_P1 - Z_M1);
        // Order 2
        K[M200] = invRho * (X_M1 + X_P1);
        K[M020] = invRho * (Y_M1 + Y_P1);
        K[M002] = invRho * (Z_M1 + Z_P1);
        K[M110] = invRho * ( fin(i, 3) - fin(i, 4) + fin(i,13) - fin(i,14));
        K[M101] = invRho * ( fin(i, 5) - fin(i, 6) + fin(i,15) - fin(i,16));
        K[M011] = invRho * ( fin(i, 7) - fin(i, 8) + fin(i,17) - fin(i,18));
        // Order 3
        K[M210] = K[M110] - two_invRho * (fin(i, 3) - fin(i, 4));
        K[M201] = K[M101] - two_invRho * (fin(i, 5) - fin(i, 6));
        K[M021] = K[M011] - two_invRho * (fin(i, 7) - fin(i, 8));
        K[M120] = K[M110] - two_invRho * (fin(i, 3) - fin(i,14));
        K[M102] = K[M101] - two_invRho * (fin(i, 5) - fin(i,16));
        K[M012] = K[M011] - two_invRho * (fin(i, 7) - fin(i,18));
        // Order 4
        K[M220] = K[M110] + two_invRho * (fin(i, 4) + fin(i,14));
        K[M202] = K[M101] + two_invRho * (fin(i, 6) + fin(i,16));
        K[M022] = K[M011] + two_invRho * (fin(i, 8) + fin(i,18));

        // Compute central moments from raw moments using binomial formulas
        double ux2 = K[M100]*K[M100];
        double uy2 = K[M010]*K[M010];
        double uz2 = K[M001]*K[M001];
        double uxy = K[M100]*K[M010];
        double uxz = K[M100]*K[M001];
        double uyz = K[M010]*K[M001];

        K[M200] -= ux2;
        K[M020] -= uy2;
        K[M002] -= uz2;
        
        K[M110] -= uxy;
        K[M101] -= uxz;
        K[M011] -= uyz;

        K[M210] -= (K[M010]*K[M200] + 2.*K[M100]*K[M110] + ux2*K[M010]);
        K[M201] -= (K[M001]*K[M200] + 2.*K[M100]*K[M101] + ux2*K[M001]);
        K[M021] -= (K[M001]*K[M020] + 2.*K[M010]*K[M011] + uy2*K[M001]);
        K[M120] -= (K[M100]*K[M020] + 2.*K[M010]*K[M110] + K[M100]*uy2);
        K[M102] -= (K[M100]*K[M002] + 2.*K[M001]*K[M101] + K[M100]*uz2);
        K[M012] -= (K[M010]*K[M002] + 2.*K[M001]*K[M011] + K[M010]*uz2);
        
        K[M220] -= (2.*K[M010]*K[M210] + 2.*K[M100]*K[M120] + uy2*K[M200] + ux2*K[M020] + 4.*uxy*K[M110] + ux2*uy2);
        K[M202] -= (2.*K[M001]*K[M201] + 2.*K[M100]*K[M102] + uz2*K[M200] + ux2*K[M002] + 4.*uxz*K[M101] + ux2*uz2);
        K[M022] -= (2.*K[M001]*K[M021] + 2.*K[M010]*K[M012] + uz2*K[M020] + uy2*K[M002] + 4.*uyz*K[M011] + uy2*uz2);

        // Compute cumulants from central moments
        K[M220] -= (K[M200]*K[M020] + 2.*K[M110]*K[M110]);
        K[M202] -= (K[M200]*K[M002] + 2.*K[M101]*K[M101]);
        K[M022] -= (K[M020]*K[M002] + 2.*K[M011]*K[M011]);

        return K;
    }

    auto computeKeq() {

        std::array<double, 19> Keq;

        double cs2 = 1./3.;
        // Order 2
        Keq[M200] = cs2;
        Keq[M020] = cs2;
        Keq[M002] = cs2;
        Keq[M110] = 0.;
        Keq[M101] = 0.;
        Keq[M011] = 0.;
        // Order 3
        Keq[M210] = 0.;
        Keq[M201] = 0.;
        Keq[M021] = 0.;
        Keq[M120] = 0.;
        Keq[M102] = 0.;
        Keq[M012] = 0.;
        // Order 4
        Keq[M220] = 0.;
        Keq[M202] = 0.;
        Keq[M022] = 0.;

        return Keq;
    }

    auto collideAndStreamK(int i, int iX, int iY, int iZ, double rho, std::array<double, 3> const& u,
                           std::array<double, 19> const& K, std::array<double, 19> const& Keq)
    {
        const double omega1 = omega;
        const double omega2 = omega;
        const double omega3 = omega;
        const double omega4 = omega;

        double cs2 = 1./3.;

        // Post-collision moments.
        std::array<double, 19> Kcoll;
        std::array<double, 19> CMcoll;
        std::array<double, 19> RMcoll;

        // Order 2
        Kcoll[M200] = (1. - omega1) * K[M200] + omega1 * cs2;
        Kcoll[M020] = (1. - omega1) * K[M020] + omega1 * cs2;
        Kcoll[M002] = (1. - omega1) * K[M002] + omega1 * cs2;

        //// To adjust the bulk viscosity, uncomment the lines below
        // const double omegaBulk = omega1;
        // const double omegaMinus = (omegaBulk - omega1)/3.; // Notation used by Fei
        // const double omegaPlus  = omegaMinus + omega1;     // Notation used by Fei
        // Kcoll[M200] = K[M200] - omegaPlus  * (K[M200]-Keq[M200]) - omegaMinus * (K[M020]-Keq[M020]) - omegaMinus * (K[M002]-Keq[M002]) ;
        // Kcoll[M020] = K[M020] - omegaMinus * (K[M200]-Keq[M200]) - omegaPlus  * (K[M020]-Keq[M020]) - omegaMinus * (K[M002]-Keq[M002]) ;
        // Kcoll[M002] = K[M002] - omegaMinus * (K[M200]-Keq[M200]) - omegaMinus * (K[M020]-Keq[M020]) - omegaPlus  * (K[M002]-Keq[M002]) ;
        
        Kcoll[M110] = (1. - omega2) * K[M110];
        Kcoll[M101] = (1. - omega2) * K[M101];
        Kcoll[M011] = (1. - omega2) * K[M011];

        // Order 3
        Kcoll[M210] = (1. - omega3) * K[M210];
        Kcoll[M201] = (1. - omega3) * K[M201];
        Kcoll[M021] = (1. - omega3) * K[M021];
        Kcoll[M120] = (1. - omega3) * K[M120];
        Kcoll[M102] = (1. - omega3) * K[M102];
        Kcoll[M012] = (1. - omega3) * K[M012];
        
        // Order 4
        Kcoll[M220] = (1. - omega4) * K[M220];
        Kcoll[M202] = (1. - omega4) * K[M202];
        Kcoll[M022] = (1. - omega4) * K[M022];

        // Come back to CMcoll modifying fourth- and higher-order post-collision cumulants
        CMcoll[M200] = Kcoll[M200];
        CMcoll[M020] = Kcoll[M020];
        CMcoll[M002] = Kcoll[M002];
        CMcoll[M110] = Kcoll[M110];
        CMcoll[M101] = Kcoll[M101];
        CMcoll[M011] = Kcoll[M011];
        
        CMcoll[M210] = Kcoll[M210];
        CMcoll[M201] = Kcoll[M201];
        CMcoll[M021] = Kcoll[M021];
        CMcoll[M120] = Kcoll[M120];
        CMcoll[M102] = Kcoll[M102];
        CMcoll[M012] = Kcoll[M012];
        
        CMcoll[M220] = Kcoll[M220] + Kcoll[M200]*Kcoll[M020] + 2.*Kcoll[M110]*Kcoll[M110];
        CMcoll[M202] = Kcoll[M202] + Kcoll[M200]*Kcoll[M002] + 2.*Kcoll[M101]*Kcoll[M101];
        CMcoll[M022] = Kcoll[M022] + Kcoll[M020]*Kcoll[M002] + 2.*Kcoll[M011]*Kcoll[M011];

        // Come back to RMcoll using binomial formulas
        double ux2 = u[0]*u[0];
        double uy2 = u[1]*u[1];
        double uz2 = u[2]*u[2];
        double uxy = u[0]*u[1];
        double uxz = u[0]*u[2];
        double uyz = u[1]*u[2];


        RMcoll[M200] = CMcoll[M200] + ux2;
        RMcoll[M020] = CMcoll[M020] + uy2;
        RMcoll[M002] = CMcoll[M002] + uz2;
        
        RMcoll[M110] = CMcoll[M110] + uxy;
        RMcoll[M101] = CMcoll[M101] + uxz;
        RMcoll[M011] = CMcoll[M011] + uyz;

        RMcoll[M210] = CMcoll[M210] + u[1]*CMcoll[M200] + 2.*u[0]*CMcoll[M110] + ux2*u[1];
        RMcoll[M201] = CMcoll[M201] + u[2]*CMcoll[M200] + 2.*u[0]*CMcoll[M101] + ux2*u[2];
        RMcoll[M021] = CMcoll[M021] + u[2]*CMcoll[M020] + 2.*u[1]*CMcoll[M011] + uy2*u[2];
        RMcoll[M120] = CMcoll[M120] + u[0]*CMcoll[M020] + 2.*u[1]*CMcoll[M110] + u[0]*uy2;
        RMcoll[M102] = CMcoll[M102] + u[0]*CMcoll[M002] + 2.*u[2]*CMcoll[M101] + u[0]*uz2;
        RMcoll[M012] = CMcoll[M012] + u[1]*CMcoll[M002] + 2.*u[2]*CMcoll[M011] + u[1]*uz2;
        
        RMcoll[M220] = CMcoll[M220] + 2.*u[1]*CMcoll[M210] + 2.*u[0]*CMcoll[M120] + uy2*CMcoll[M200] + ux2*CMcoll[M020] + 4.*uxy*CMcoll[M110] + ux2*uy2;
        RMcoll[M202] = CMcoll[M202] + 2.*u[2]*CMcoll[M201] + 2.*u[0]*CMcoll[M102] + uz2*CMcoll[M200] + ux2*CMcoll[M002] + 4.*uxz*CMcoll[M101] + ux2*uz2;
        RMcoll[M022] = CMcoll[M022] + 2.*u[2]*CMcoll[M021] + 2.*u[1]*CMcoll[M012] + uz2*CMcoll[M020] + uy2*CMcoll[M002] + 4.*uyz*CMcoll[M011] + uy2*uz2;

        // Optimization based on symmetries between populations and their opposite counterpart
        double pop_out_09 = rho *(1. - RMcoll[M200] - RMcoll[M020] - RMcoll[M002] + RMcoll[M220] + RMcoll[M202] + RMcoll[M022]);
        
        double pop_out_opp_00 = 0.5*rho * ( u[0] + RMcoll[M200] - RMcoll[M120] - RMcoll[M102] - RMcoll[M220] - RMcoll[M202]);
        double pop_out_00     =     rho * (-u[0]                + RMcoll[M120] + RMcoll[M102]) + pop_out_opp_00;

        double pop_out_opp_01 = 0.5*rho * ( u[1] + RMcoll[M020] - RMcoll[M210] - RMcoll[M012] - RMcoll[M220] - RMcoll[M022]);
        double pop_out_01     =     rho * (-u[1]                + RMcoll[M210] + RMcoll[M012]) + pop_out_opp_01;

        double pop_out_opp_02 = 0.5*rho * ( u[2] + RMcoll[M002] - RMcoll[M201] - RMcoll[M021] - RMcoll[M202] - RMcoll[M022]);
        double pop_out_02     =     rho * (-u[2]                + RMcoll[M201] + RMcoll[M021]) + pop_out_opp_02;

        double pop_out_opp_03 = 0.25*rho * ( RMcoll[M110] + RMcoll[M210] + RMcoll[M120] + RMcoll[M220]);
        double pop_out_04     =  0.5*rho * (-RMcoll[M110]                - RMcoll[M120]) + pop_out_opp_03;
        double pop_out_opp_04 =  0.5*rho * (-RMcoll[M110] - RMcoll[M210])                + pop_out_opp_03;
        double pop_out_03     =  0.5*rho * (              - RMcoll[M210] - RMcoll[M120]) + pop_out_opp_03;

        double pop_out_opp_05 = 0.25*rho * ( RMcoll[M101] + RMcoll[M201] + RMcoll[M102] + RMcoll[M202]);
        double pop_out_06     =  0.5*rho * (-RMcoll[M101]                - RMcoll[M102]) + pop_out_opp_05;
        double pop_out_opp_06 =  0.5*rho * (-RMcoll[M101] - RMcoll[M201])                + pop_out_opp_05;
        double pop_out_05     =  0.5*rho * (              - RMcoll[M201] - RMcoll[M102]) + pop_out_opp_05;

        double pop_out_opp_07 = 0.25*rho * ( RMcoll[M011] + RMcoll[M021] + RMcoll[M012] + RMcoll[M022]);
        double pop_out_08     =  0.5*rho * (-RMcoll[M011]                - RMcoll[M012]) + pop_out_opp_07;
        double pop_out_opp_08 =  0.5*rho * (-RMcoll[M011] - RMcoll[M021])                + pop_out_opp_07;
        double pop_out_07     =  0.5*rho * (              - RMcoll[M021] - RMcoll[M012]) + pop_out_opp_07;


        int XX = iX - 1;
        int YY = iY;
        int ZZ = iZ;
        size_t nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 10) = pop_out_00 + f(nb, 0);
        }
        else {
            fout(nb, 0) = pop_out_00;
        }

        XX = iX + 1;
        YY = iY;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 0) = pop_out_opp_00 + f(nb,10);
        }
        else {
            fout(nb,10) = pop_out_opp_00;
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 11) = pop_out_01 + f(nb, 1);
        }
        else {
            fout(nb, 1) = pop_out_01;
        }

        XX = iX;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 1) = pop_out_opp_01 + f(nb,11);
        }
        else {
            fout(nb,11) = pop_out_opp_01;
        }

        XX = iX;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 12) = pop_out_02 + f(nb, 2);
        }
        else {
            fout(nb, 2) = pop_out_02;
        }

        XX = iX;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 2) = pop_out_opp_02 + f(nb,12);
        }
        else {
            fout(nb,12) = pop_out_opp_02;
        }

        XX = iX - 1;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 13) = pop_out_03 + f(nb, 3);
        }
        else {
            fout(nb, 3) = pop_out_03;
        }

        XX = iX + 1;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 3) = pop_out_opp_03 + f(nb,13);
        }
        else {
            fout(nb,13) = pop_out_opp_03;
        }

        XX = iX - 1;
        YY = iY + 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 14) = pop_out_04 + f(nb, 4);
        }
        else {
            fout(nb, 4) = pop_out_04;
        }

        XX = iX + 1;
        YY = iY - 1;
        ZZ = iZ;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 4) = pop_out_opp_04 + f(nb,14);
        }
        else {
            fout(nb,14) = pop_out_opp_04;
        }

        XX = iX - 1;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 15) = pop_out_05 + f(nb, 5);
        }
        else {
            fout(nb, 5) = pop_out_05;
        }

        XX = iX + 1;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 5) = pop_out_opp_05 + f(nb,15);
        }
        else {
            fout(nb,15) = pop_out_opp_05;
        }

        XX = iX - 1;
        YY = iY;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 16) = pop_out_06 + f(nb, 6);
        }
        else {
            fout(nb, 6) = pop_out_06;
        }

        XX = iX + 1;
        YY = iY;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 6) = pop_out_opp_06 + f(nb,16);
        }
        else {
            fout(nb,16) = pop_out_opp_06;
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 17) = pop_out_07 + f(nb, 7);
        }
        else {
            fout(nb, 7) = pop_out_07;
        }

        XX = iX;
        YY = iY + 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 7) = pop_out_opp_07 + f(nb,17);
        }
        else {
            fout(nb,17) = pop_out_opp_07;
        }

        XX = iX;
        YY = iY - 1;
        ZZ = iZ + 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 18) = pop_out_08 + f(nb, 8);
        }
        else {
            fout(nb, 8) = pop_out_08;
        }

        XX = iX;
        YY = iY + 1;
        ZZ = iZ - 1;
        nb = xyz_to_i(XX, YY, ZZ);
        if (flag[nb] == CellType::bounce_back) {
            fout(i, 8) = pop_out_opp_08 + f(nb,18);
        }
        else {
            fout(nb,18) = pop_out_opp_08;
        }


        fout(i, 9) =  pop_out_09;
    }


    void iterateK(double& f0) {
        int i = &f0 - lattice;
        if (flag[i] == CellType::bulk) {
            auto[iX, iY, iZ] = i_to_xyz(i);
            // auto[rho, u] = macro(f0);
            // auto K = computeK(f0, rho, u);
            // auto K = computeKopt(f0, rho, u);
            auto K = computeKopt2(f0);
            double rho = K[M000];
            std::array<double, 3> u = {K[M100], K[M010], K[M001]}; 
            auto Keq = computeKeq();
            collideAndStreamK(i, iX, iY, iZ, rho, u, K, Keq);
        }
    }

    void operator() (double& f0) {
        iterateK(f0);
    }
};

} // namespace twopop_soa_k


